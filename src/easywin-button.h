﻿#pragma once

#include <memory>
#include <string>

#include "types.h"
#include "platform.h"

#include "easywin-commandwindow.h"
#include "easywin-window.h"
#include "easywin-bitmap.h"
#include "easywin-rect.h"

namespace easywin
{
	class Button : public CommandWindow
	{
	public:
		typedef std::shared_ptr<Button> Ptr;
		typedef std::weak_ptr<Button> WPtr;

		static Button::Ptr Create(const Window::Ptr& parent, const std::wstring& title, const Rect& rect, DWORD style);

		static Button::Ptr CreateButton(const Window::Ptr& parent, const std::wstring& title, const Rect& rect) { return Create(parent, title, rect, BS_NOTIFY | WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON); }

		static Button::Ptr CreateButton(const Window::Ptr& parent, const std::wstring& title, const SPBitmap& bitmap, const Rect& rect);

		static Button::Ptr CreateCheckbox(const Window::Ptr& parent, const std::wstring& title, const Rect& rect) { return Create(parent, title, rect, BS_NOTIFY | WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX); }

		static Button::Ptr CreateRadio(const Window::Ptr& parent, const std::wstring& title, const Rect& rect, bool firstInGroup) { return Create(parent, title, rect, BS_NOTIFY | WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_AUTORADIOBUTTON | (firstInGroup ? WS_GROUP : 0)); }

		bool isChecked();
		void setCheck(bool checked);

		[[deprecated("Use setOnClick instead.")]]
		void setOnCommand(const std::function<void()>& fun) { CommandWindow::setOnCommand(BN_CLICKED, fun); }

		virtual void setOnClick(const std::function<void()>& fun) override { CommandWindow::setOnCommand(BN_CLICKED, fun); }
		virtual void setOnSetFocus(const std::function<void()>& fun) override { CommandWindow::setOnCommand(BN_SETFOCUS, fun); }
		virtual void setOnKillFocus(const std::function<void()>& fun) override { CommandWindow::setOnCommand(BN_KILLFOCUS, fun); }

		void setBitmap(const Bitmap::Ptr& bitmap) override;
	protected:
		Button::Ptr getReference() { return std::dynamic_pointer_cast<Button>(shared_from_this()); }
	};
}
