#pragma once

#include <string>
#include <memory>
#include <functional>
#include <vector>

#include "types.h"
#include "platform.h"

#include "easywin-commandwindow.h"
#include "easywin-window.h"

namespace easywin
{
	class Tab : public CommandWindow
	{
	public:
		typedef std::shared_ptr<Tab> Ptr;

		static Ptr Create(WindowBase::Ptr parent, const RECT& rect);

		Window::Ptr addItem(const std::u8string& name);

		bool removeTab(Window::Ptr tabClient);

		int getSelectedIndex();
		Window::Ptr getSelectedWindow();

		virtual void updateLayout();
		void updateSelection();

		virtual int onNotify(unsigned int code);

		virtual void setOnChange(const std::function<void()>& fun) override { m_onChange = fun; }

	protected:
		SPTab getReference() { return std::dynamic_pointer_cast<Tab>(shared_from_this()); }
	private:
		Tab() {}
		std::vector<Window::Ptr> m_tabClients;

		std::function<void()> m_onChange;
	};
}