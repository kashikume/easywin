#pragma once

#include <compare>
#include <string>
#include <memory>
#include <vector>
#include <compare>
#include <optional>
#include <filesystem>

#include "types.h"
#include "platform.h"

#include "easywin-window.h"

namespace easywin
{
	class FileDialog
	{
	public:
		typedef std::vector< std::pair<std::wstring, std::wstring> > selector_type;

		FileDialog();

		void setFileTypes(const selector_type& fileTypes, int defaultSelection, const std::wstring& defaultExtension) { fileTypesSelectorData.update(fileTypes, defaultSelection + 1, defaultExtension); }
		void setFolder(const std::filesystem::path& path);
		void setFileName(const std::wstring& filename);

		bool showFileOpenDialog(const Window::Ptr& parent);
		bool showFileSaveDialog(const Window::Ptr& parent);
		bool showFolderSelectDialog(const Window::Ptr& parent);

		const std::filesystem::path& getResult() const { return m_result; }

	private:
		class SelectorData
		{
		private:
			std::vector< COMDLG_FILTERSPEC > data;
			int defaultSelection = 1;
			std::wstring defaultExtension;
			void release();
		public:
			SelectorData() {}
			SelectorData(const selector_type& data);
			SelectorData(const SelectorData&) = delete;
			SelectorData operator = (const SelectorData&) = delete;
			~SelectorData();
			void update(const selector_type& data, int defaultSelection, const std::wstring& defaultExtension);
			COMDLG_FILTERSPEC* get() { return data.data(); }
			size_t size() { return data.size(); }
			int getDefaultSelection() { return defaultSelection; }
			const std::wstring& getDefaultExtension() { return defaultExtension; }
		};
		SelectorData fileTypesSelectorData;
		std::optional<std::filesystem::path> m_folder;
		std::optional<std::wstring> m_fileName;

		std::filesystem::path m_result;

		HRESULT CreateInstance(REFIID riid, void** ppv);

		bool showDialog(const Window::Ptr& parent, const IID& rclsid, DWORD flags);
	};
}