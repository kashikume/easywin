#include <memory>
#include <Windows.h>
#include <string>
#include <cassert>
#include <sstream>

#include "types.h"

#include "easywin-windowbase.h"

#include "easywin-utf8.h"
#include "easywin-app.h"

#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

namespace easywin
{
	void WindowBase::setFocus()
	{
		::SetFocus(m_hwnd);
	}

	void WindowBase::setSize(int w, int h)
	{
		SetWindowPos(m_hwnd, NULL, 0, 0, w, h, SWP_NOMOVE | SWP_NOZORDER);
		updateLayout();
	}

	void WindowBase::moveWindow(const RECT& rc, bool repaint)
	{
		::MoveWindow(m_hwnd, rc.left, rc.top, rc.right - rc.left, rc.bottom - rc.top, repaint);
		updateLayout();
	}

	void WindowBase::moveWindow(int x, int y, int w, int h, bool repaint)
	{
		::MoveWindow(m_hwnd, x, y, w, h, repaint);
		updateLayout();
	}

	Rect WindowBase::getWindowRect()
	{
		Rect r;
		GetWindowRect(m_hwnd, &r);
		return r;
	}

	Rect WindowBase::getClientRect()
	{
		Rect r;
		GetClientRect(m_hwnd, &r);
		return r;
	}

	void WindowBase::destroy()
	{
		auto keep = getWindowBase(); // keep reference to avoid premature destruction
		while (!m_children.empty()) {
			(*(m_children.begin()))->destroy();
		}

		if (m_hwnd) DestroyWindow(m_hwnd);
		removeSelf();
	}

	int WindowBase::getDpiForWindow()
	{
		return GetDpiForWindow(m_hwnd);
	}

	FontSetup WindowBase::getDefaultFontSetup()
	{
		NONCLIENTMETRICS metrics {};
		metrics.cbSize = sizeof(NONCLIENTMETRICS);

		auto dpi = getDpiForWindow();
		::SystemParametersInfoForDpi(SPI_GETNONCLIENTMETRICS, sizeof(NONCLIENTMETRICS), &metrics, 0, dpi);

		FontSetup fs;
		fs.height = metrics.lfMessageFont.lfHeight;
		fs.width = metrics.lfMessageFont.lfWidth;
		fs.escapement = metrics.lfMessageFont.lfEscapement;
		fs.orientation = metrics.lfMessageFont.lfOrientation;
		fs.weight = metrics.lfMessageFont.lfWeight;
		fs.italic = metrics.lfMessageFont.lfItalic != 0;
		fs.underline = metrics.lfMessageFont.lfUnderline != 0;
		fs.strikeOut = metrics.lfMessageFont.lfStrikeOut != 0;
		fs.charSet = metrics.lfMessageFont.lfCharSet;
		fs.outPrecision = metrics.lfMessageFont.lfOutPrecision;
		fs.clipPrecision = metrics.lfMessageFont.lfClipPrecision;
		fs.quality = metrics.lfMessageFont.lfQuality;
		fs.pitchAndFamily = metrics.lfMessageFont.lfPitchAndFamily;
		fs.u8FontFaceName = wStrToUtf8(metrics.lfMessageFont.lfFaceName);
		fs.dpi = dpi;
		return fs;
	}

	void WindowBase::setFont(const Font::Ptr& font)
	{
		m_font = font;
		::SendMessage(m_hwnd, WM_SETFONT, (WPARAM)m_font->getHFont(), MAKELPARAM(TRUE, 0));
	}

	Font::Ptr WindowBase::getFont()
	{
		if (!m_font) setupDefaultFont();
		return m_font;
	}

	void WindowBase::setupDefaultFont()
	{
		setFont(Font::Create(getDefaultFontSetup()));
	}

	bool WindowBase::createWindow(const std::wstring& className, const std::wstring& windowName, DWORD dwStyle, const Rect& rect, WindowBase::Ptr parent, DWORD dwExStyle, LPVOID lpParam)
	{
		m_windowClassName = className;

		m_hwnd = ::CreateWindowExW(
			dwExStyle,
			m_windowClassName.c_str(),
			windowName.c_str(),
			dwStyle,
			rect.left,
			rect.left == CW_USEDEFAULT ? 0 : rect.top,
			rect.right == CW_USEDEFAULT ? CW_USEDEFAULT : rect.width(),
			rect.right == CW_USEDEFAULT ? 0 : rect.height(),
			parent ? parent->getHWnd() : NULL,
			NULL,
			GetModuleHandle(NULL),
			lpParam);

		theApp::getInstance().addWindow(getWindowBase());


		if (parent) {
			parent->addChild(getWindowBase());
			this->m_parent = parent;
		}

		return m_hwnd != NULL;
	}

	void WindowBase::removeSelf()
	{
		auto keep = getWindowBase(); // keep reference to avoid premature destruction
		if (m_hwnd) {
			auto p = m_parent.lock();
			if (p) p->removeChild(m_hwnd);
			m_parent.reset();
			theApp::getInstance().removeWindow(m_hwnd);
			m_hwnd = NULL;
		}

		if (m_windowClassName.empty() == false) {
			::UnregisterClassW(m_windowClassName.c_str(), ::GetModuleHandle(NULL));
		}
	}

	int WindowBase::showMessageBox(const std::wstring& text, const std::wstring& caption, UINT type)
	{
		return ::MessageBoxW(m_hwnd,text.c_str(), caption.c_str(), type);
	}

	void WindowBase::addChild(const WindowBase::Ptr& win)
	{
		assert(win);
		m_children.push_back(win);
	}

	void WindowBase::removeChild(HWND childHwnd)
	{
		m_children.remove_if([childHwnd](const WindowBase::Ptr& c) {
			return c->getHWnd() == childHwnd;
			});
	}

	WindowBase::Ptr WindowBase::getTopParent()
	{
		auto parent = getParent();
		if (parent) return parent->getTopParent();
		return getWindowBase();
	}

	std::wstring WindowBase::describe()
	{
		std::wstringstream ss;
		ss << m_hwnd << L" : " << m_windowClassName;
		return ss.str();
	}

	void WindowBase::postMessage(UINT message, WPARAM wParam, LPARAM lParam)
	{
		::PostMessageW(m_hwnd, message, wParam, lParam);
	}

	WindowBase::~WindowBase()
	{
		assert(m_hwnd == NULL);
	}

	void WindowBase::setText(const std::u8string& text)
	{
		::SetWindowTextW(m_hwnd, utf8ToWStr(text).c_str());
	}

	void WindowBase::setText(const std::wstring& text)
	{
		::SetWindowTextW(m_hwnd, text.c_str());
	}

	std::wstring WindowBase::getText()
	{
		auto len = ::GetWindowTextLengthW(m_hwnd) + 1;
		std::wstring txt;
		txt.resize(len);
		::GetWindowTextW(m_hwnd, txt.data(), len);
		return std::wstring(txt.c_str());
	}

	std::u8string WindowBase::getTextU8()
	{
		return wStrToUtf8(getText());
	}

	void WindowBase::setBitmap(const Bitmap::Ptr& bitmap)
	{
		assert(false);
	}

}
