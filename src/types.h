﻿#pragma once

#include <memory>

namespace easywin
{
#define EW_DECL(cl) \
	class cl; \
	typedef std::shared_ptr<cl> cl##_Ptr; \
	typedef std::weak_ptr<cl> cl##_WPtr; \
	typedef std::shared_ptr<cl> SP##cl; \
	typedef std::weak_ptr<cl> WP##cl;

	EW_DECL(Bitmap);
	EW_DECL(Button);
	EW_DECL(CommandWindow);
	EW_DECL(Cursor);
	EW_DECL(Edit);
	EW_DECL(Font);
	EW_DECL(Icon);
	EW_DECL(Menu);
	EW_DECL(MenuChangeListener);
	EW_DECL(PaintDC);
	EW_DECL(ScrollBar);
	EW_DECL(StaticText);
	EW_DECL(Tab);
	EW_DECL(Trackbar);
	EW_DECL(Window);
	EW_DECL(WindowBase);
}