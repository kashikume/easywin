#include <string>

#include "platform.h"
#include "types.h"

#include "easywin-tab.h"

#include "easywin-windowbase.h"
#include "easywin-window.h"
#include "easywin-utf8.h"

namespace easywin
{
	SPTab Tab::Create(WindowBase::Ptr parent, const RECT& rect)
	{
		SPTab tab = SPTab(new Tab);

		INITCOMMONCONTROLSEX icex;

		// Initialize common controls.
		icex.dwSize = sizeof(INITCOMMONCONTROLSEX);
		icex.dwICC = ICC_TAB_CLASSES;
		InitCommonControlsEx(&icex);

		tab->WindowBase::createWindow(
			WC_TABCONTROLW,
			L"",
			WS_CHILD | WS_CLIPSIBLINGS | WS_VISIBLE,
			rect,
			parent);

		tab->setupDefaultFont();

		return tab;
	}

	Window::Ptr Tab::addItem(const std::u8string& name)
	{
		TCITEM tie;
		tie.mask = TCIF_TEXT;
		auto t2 = utf8ToWStr(name);
		tie.pszText = (LPWSTR)t2.c_str();
		TabCtrl_InsertItem(m_hwnd, m_tabClients.size(), &tie);

		auto s = std::dynamic_pointer_cast<WindowBase>(shared_from_this());
		RECT rc = getClientRect();
		TabCtrl_AdjustRect(m_hwnd, false, &rc);
		auto tab = Window::CreateClient(s, rc);
		m_tabClients.emplace_back(tab);

		updateSelection();
		return tab;
	}

	bool Tab::removeTab(Window::Ptr tabClient)
	{
		int pos = -1;
		for (int i = 0; i < m_tabClients.size(); ++i) {
			if (tabClient == m_tabClients[i]) {
				pos = i;
				break;
			}
		}
		if (pos == -1) return false;

		TabCtrl_DeleteItem(m_hwnd, pos);
		m_tabClients.erase(m_tabClients.begin() + pos);

		updateSelection();

		return true;
	}

	int Tab::getSelectedIndex()
	{
		return TabCtrl_GetCurSel(m_hwnd);
	}

	Window::Ptr Tab::getSelectedWindow()
	{
		int ind = getSelectedIndex();
		if (ind >= 0 && ind < m_tabClients.size())
			return m_tabClients[ind];
		else
			return nullptr;
	}

	void Tab::updateLayout()
	{
		RECT rc = getClientRect();
		TabCtrl_AdjustRect(m_hwnd, false, &rc);
		for (auto& c : m_tabClients) {
			c->moveWindow(rc);
		}
	}

	void Tab::updateSelection()
	{
		int sel = TabCtrl_GetCurSel(m_hwnd);
		for (size_t i = 0; i < m_tabClients.size(); ++i) {
			if (i == sel)
				m_tabClients[i]->show();
			else
				m_tabClients[i]->hide();
		}
	}

	int Tab::onNotify(unsigned int code)
	{
		switch (code) {
		case TCN_SELCHANGE:
			updateSelection();
			if (m_onChange) m_onChange();
			break;
		}
		return CommandWindow::onNotify(code);
	}
}