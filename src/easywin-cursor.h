#pragma once

#include "types.h"
#include "platform.h"

namespace easywin
{
	class Cursor
	{
	public:
		typedef std::shared_ptr<Cursor> Ptr;

		static Ptr CreateArrow();
		static Ptr CreateAppStarting();
		static Ptr CreateCross();
		static Ptr CreateHand();
		static Ptr CreateHelp();
		static Ptr CreateIBeam();
		static Ptr CreateNo();
		static Ptr CreateSizeAll();
		static Ptr CreateSizeNESW();
		static Ptr CreateSizeNS();
		static Ptr CreateSizeNWSE();
		static Ptr CreateSizeWE();
		static Ptr CreateUpArrow();
		static Ptr CreateWait();

		void set();

		~Cursor();
	private:

		static Ptr CreateSystemCursor(LPCWSTR cursorName);

		HCURSOR m_hcursor = NULL;
		bool m_requiresRemoval = false;
	};
}