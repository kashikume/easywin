﻿#pragma once

#include <string>
#include <vector>
#include <memory>

#include "platform.h"
#include "types.h"

#include "easywin-rect.h"

namespace easywin
{
	namespace Color {
		const COLORREF maroon = RGB(128, 0, 0);
		const COLORREF dark_red = RGB(139, 0, 0);
		const COLORREF brown = RGB(165, 42, 42);
		const COLORREF firebrick = RGB(178, 34, 34);
		const COLORREF crimson = RGB(220, 20, 60);
		const COLORREF red = RGB(255, 0, 0);
		const COLORREF light_red = RGB(255, 128, 128);
		const COLORREF tomato = RGB(255, 99, 71);
		const COLORREF coral = RGB(255, 127, 80);
		const COLORREF indian_red = RGB(205, 92, 92);
		const COLORREF light_coral = RGB(240, 128, 128);
		const COLORREF dark_salmon = RGB(233, 150, 122);
		const COLORREF salmon = RGB(250, 128, 114);
		const COLORREF light_salmon = RGB(255, 160, 122);
		const COLORREF orange_red = RGB(255, 69, 0);
		const COLORREF dark_orange = RGB(255, 140, 0);
		const COLORREF orange = RGB(255, 165, 0);
		const COLORREF gold = RGB(255, 215, 0);
		const COLORREF dark_golden_rod = RGB(184, 134, 11);
		const COLORREF golden_rod = RGB(218, 165, 32);
		const COLORREF pale_golden_rod = RGB(238, 232, 170);
		const COLORREF dark_khaki = RGB(189, 183, 107);
		const COLORREF khaki = RGB(240, 230, 140);
		const COLORREF olive = RGB(128, 128, 0);
		const COLORREF yellow = RGB(255, 255, 0);
		const COLORREF yellow_green = RGB(154, 205, 50);
		const COLORREF dark_olive_green = RGB(85, 107, 47);
		const COLORREF olive_drab = RGB(107, 142, 35);
		const COLORREF lawn_green = RGB(124, 252, 0);
		const COLORREF chart_reuse = RGB(127, 255, 0);
		const COLORREF green_yellow = RGB(173, 255, 47);
		const COLORREF dark_green = RGB(0, 100, 0);
		const COLORREF green = RGB(0, 128, 0);
		const COLORREF forest_green = RGB(34, 139, 34);
		const COLORREF lime = RGB(0, 255, 0);
		const COLORREF lime_green = RGB(50, 205, 50);
		const COLORREF light_green = RGB(144, 238, 144);
		const COLORREF pale_green = RGB(152, 251, 152);
		const COLORREF dark_sea_green = RGB(143, 188, 143);
		const COLORREF medium_spring_green = RGB(0, 250, 154);
		const COLORREF spring_green = RGB(0, 255, 127);
		const COLORREF sea_green = RGB(46, 139, 87);
		const COLORREF medium_aqua_marine = RGB(102, 205, 170);
		const COLORREF medium_sea_green = RGB(60, 179, 113);
		const COLORREF light_sea_green = RGB(32, 178, 170);
		const COLORREF dark_slate_gray = RGB(47, 79, 79);
		const COLORREF teal = RGB(0, 128, 128);
		const COLORREF dark_cyan = RGB(0, 139, 139);
		const COLORREF aqua = RGB(0, 255, 255);
		const COLORREF cyan = RGB(0, 255, 255);
		const COLORREF light_cyan = RGB(224, 255, 255);
		const COLORREF dark_turquoise = RGB(0, 206, 209);
		const COLORREF turquoise = RGB(64, 224, 208);
		const COLORREF medium_turquoise = RGB(72, 209, 204);
		const COLORREF pale_turquoise = RGB(175, 238, 238);
		const COLORREF aqua_marine = RGB(127, 255, 212);
		const COLORREF powder_blue = RGB(176, 224, 230);
		const COLORREF cadet_blue = RGB(95, 158, 160);
		const COLORREF steel_blue = RGB(70, 130, 180);
		const COLORREF corn_flower_blue = RGB(100, 149, 237);
		const COLORREF deep_sky_blue = RGB(0, 191, 255);
		const COLORREF dodger_blue = RGB(30, 144, 255);
		const COLORREF light_blue = RGB(173, 216, 230);
		const COLORREF sky_blue = RGB(135, 206, 235);
		const COLORREF light_sky_blue = RGB(135, 206, 250);
		const COLORREF midnight_blue = RGB(25, 25, 112);
		const COLORREF navy = RGB(0, 0, 128);
		const COLORREF dark_blue = RGB(0, 0, 139);
		const COLORREF medium_blue = RGB(0, 0, 205);
		const COLORREF blue = RGB(0, 0, 255);
		const COLORREF royal_blue = RGB(65, 105, 225);
		const COLORREF blue_violet = RGB(138, 43, 226);
		const COLORREF indigo = RGB(75, 0, 130);
		const COLORREF dark_slate_blue = RGB(72, 61, 139);
		const COLORREF slate_blue = RGB(106, 90, 205);
		const COLORREF medium_slate_blue = RGB(123, 104, 238);
		const COLORREF medium_purple = RGB(147, 112, 219);
		const COLORREF dark_magenta = RGB(139, 0, 139);
		const COLORREF dark_violet = RGB(148, 0, 211);
		const COLORREF dark_orchid = RGB(153, 50, 204);
		const COLORREF medium_orchid = RGB(186, 85, 211);
		const COLORREF purple = RGB(128, 0, 128);
		const COLORREF thistle = RGB(216, 191, 216);
		const COLORREF plum = RGB(221, 160, 221);
		const COLORREF violet = RGB(238, 130, 238);
		const COLORREF magenta = RGB(255, 0, 255);
		const COLORREF fuchsia = magenta;
		const COLORREF orchid = RGB(218, 112, 214);
		const COLORREF medium_violet_red = RGB(199, 21, 133);
		const COLORREF pale_violet_red = RGB(219, 112, 147);
		const COLORREF deep_pink = RGB(255, 20, 147);
		const COLORREF hot_pink = RGB(255, 105, 180);
		const COLORREF light_pink = RGB(255, 182, 193);
		const COLORREF pink = RGB(255, 192, 203);
		const COLORREF antique_white = RGB(250, 235, 215);
		const COLORREF beige = RGB(245, 245, 220);
		const COLORREF bisque = RGB(255, 228, 196);
		const COLORREF blanched_almond = RGB(255, 235, 205);
		const COLORREF wheat = RGB(245, 222, 179);
		const COLORREF corn_silk = RGB(255, 248, 220);
		const COLORREF lemon_chiffon = RGB(255, 250, 205);
		const COLORREF light_golden_rod_yellow = RGB(250, 250, 210);
		const COLORREF light_yellow = RGB(255, 255, 224);
		const COLORREF saddle_brown = RGB(139, 69, 19);
		const COLORREF sienna = RGB(160, 82, 45);
		const COLORREF chocolate = RGB(210, 105, 30);
		const COLORREF peru = RGB(205, 133, 63);
		const COLORREF sandy_brown = RGB(244, 164, 96);
		const COLORREF burly_wood = RGB(222, 184, 135);
		const COLORREF tan = RGB(210, 180, 140);
		const COLORREF rosy_brown = RGB(188, 143, 143);
		const COLORREF moccasin = RGB(255, 228, 181);
		const COLORREF navajo_white = RGB(255, 222, 173);
		const COLORREF peach_puff = RGB(255, 218, 185);
		const COLORREF misty_rose = RGB(255, 228, 225);
		const COLORREF lavender_blush = RGB(255, 240, 245);
		const COLORREF linen = RGB(250, 240, 230);
		const COLORREF old_lace = RGB(253, 245, 230);
		const COLORREF papaya_whip = RGB(255, 239, 213);
		const COLORREF sea_shell = RGB(255, 245, 238);
		const COLORREF mint_cream = RGB(245, 255, 250);
		const COLORREF slate_gray = RGB(112, 128, 144);
		const COLORREF light_slate_gray = RGB(119, 136, 153);
		const COLORREF light_steel_blue = RGB(176, 196, 222);
		const COLORREF lavender = RGB(230, 230, 250);
		const COLORREF floral_white = RGB(255, 250, 240);
		const COLORREF alice_blue = RGB(240, 248, 255);
		const COLORREF ghost_white = RGB(248, 248, 255);
		const COLORREF honeydew = RGB(240, 255, 240);
		const COLORREF ivory = RGB(255, 255, 240);
		const COLORREF azure = RGB(240, 255, 255);
		const COLORREF snow = RGB(255, 250, 250);
		const COLORREF black = RGB(0, 0, 0);
		const COLORREF dim_gray = RGB(105, 105, 105);
		const COLORREF dim_grey = dim_gray;
		const COLORREF gray = RGB(128, 128, 128);
		const COLORREF grey = gray;
		const COLORREF dark_gray = RGB(169, 169, 169);
		const COLORREF dark_grey = dark_gray;
		const COLORREF silver = RGB(192, 192, 192);
		const COLORREF light_gray = RGB(211, 211, 211);
		const COLORREF light_grey = light_gray;
		const COLORREF gainsboro = RGB(220, 220, 220);
		const COLORREF white_smoke = RGB(245, 245, 245);
		const COLORREF white = RGB(255, 255, 255);
	}


	class PaintDC {
	public:
		typedef std::shared_ptr<PaintDC> Ptr;
		typedef std::weak_ptr<PaintDC> WPtr;

		PaintDC(HWND hwnd);
		PaintDC(HWND hwnd, HDC hdc);
		PaintDC(HDC hdc);
		virtual ~PaintDC();

		void drawCircle(int x, int y, int r);
		void drawRectangle(int x1, int y1, int x2, int y2);
		void drawRectangle(const Rect& rect);
		void drawRoundedRectangle(const Rect& rect, int rw, int rh);
		void drawLine(int x1, int y1, int x2, int y2);
		void drawBitmap(int x, int y, Bitmap& bitmap);
		void drawBitmap(int x, int y, const Bitmap_Ptr& bitmap);
		void drawBitmap(const Rect& dst, const Bitmap_Ptr& bitmap);
		void alphaBlendBitmap(int x, int y, Bitmap& bitmap, int alpha = 255);
		void alphaBlendBitmap(int x, int y, const Bitmap_Ptr& bitmap, int alpha = 255);
		void alphaBlendBitmap(const Rect& dst, const Bitmap_Ptr& bitmap, int alpha = 255);
		void drawIcon(int x, int y, const Icon_Ptr& icon);
		void setBrushColor(COLORREF color) { SetDCBrushColor(m_hdc, color); }
		void setPenColor(COLORREF color) { SetDCPenColor(m_hdc, color); }
		void textOut(int x, int y, const std::string& text);
		void textOut(int x, int y, const std::u8string& text);
		void textOut(int x, int y, const std::wstring& text);

		bool swapBuffers() { return SwapBuffers(m_hdc); }

		COLORREF getPixel(int x, int y);
		void setPixel(int x, int y, COLORREF color);

		void selectStockBrush();
		void selectStockPen();

		enum class TextFormatConst
		{
			alignTop,
			alignCenter,
			alignBottom,
			alignLeft = alignTop,
			alignRight = alignBottom,
			multiLine,
			singleLine,

		};

		struct TextFormat
		{
			TextFormatConst vAlign = TextFormatConst::alignTop;
			TextFormatConst hAlign = TextFormatConst::alignLeft;
			TextFormatConst lines = TextFormatConst::singleLine;
			bool transparentBackground = false;
			COLORREF backgroundColor = Color::white;
			COLORREF textColor = Color::black;
		};

		void drawText(const Font_Ptr& font, Rect rc, std::string u8Text, const TextFormat& format);

		PaintDC::Ptr createCompatibile(Bitmap& bitmap);
		HDC getHdc() const { return m_hdc; }
		operator HDC() const { return m_hdc; }

		static PaintDC::Ptr getFromWindow(const Window_Ptr& window);
	protected:
	private:
		HWND m_hwnd;
		HDC m_hdc;
		PAINTSTRUCT m_ps;
		bool m_callEndPaint;
		bool m_requiresDeletation;
		std::vector<HGDIOBJ> m_oldObj;
	};

}