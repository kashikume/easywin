#include <functional>
#include <cassert>

#include "platform.h"

#define TESTING_WM_NOTIFY 0

#include "easywin-app.h"
#include "easywin-commandwindow.h"
#include "easywin-utf8.h"

namespace easywin
{
	void CommandWindow::addTooltip(const std::wstring& text, bool baloon)
	{
		m_tooltipWindowHandle = CreateWindowEx(NULL, TOOLTIPS_CLASS, NULL,
			WS_POPUP | TTS_ALWAYSTIP | (baloon ? TTS_BALLOON : 0),
			CW_USEDEFAULT, CW_USEDEFAULT,
			CW_USEDEFAULT, CW_USEDEFAULT,
			m_hwnd, NULL,
			GetModuleHandle(NULL), NULL);

		if (!m_tooltipWindowHandle) {
			return;
		}

		// Associate the tooltip with the tool.
		TOOLINFO toolInfo = {};
		toolInfo.cbSize = sizeof(toolInfo);
		toolInfo.hwnd = m_hwnd;
		toolInfo.uFlags = TTF_IDISHWND | TTF_SUBCLASS;
		toolInfo.uId = (UINT_PTR)m_hwnd;
		toolInfo.lpszText = (WCHAR*)text.c_str();
		SendMessage(m_tooltipWindowHandle, TTM_ADDTOOL, 0, (LPARAM)&toolInfo);
	}

	void CommandWindow::addTooltip(const std::u8string& text, bool baloon)
	{
		addTooltip(utf8ToWStr(text), baloon);
	}

	void CommandWindow::removeTooltip()
	{
		if (m_tooltipWindowHandle) {
			::DestroyWindow(m_tooltipWindowHandle);
			m_tooltipWindowHandle = NULL;
		}
	}

	void CommandWindow::setOnCommand(unsigned notification, std::function<void()> fun)
	{
		if (fun)
			m_onCommandCb[notification] = fun;
		else
			m_onCommandCb.erase(notification);
	}

	int CommandWindow::callOnCommand(unsigned notification)
	{
		auto fun = m_onCommandCb.find(notification);
		if (fun == m_onCommandCb.end()) return 1;
		fun->second();
		return 0;
	}

	CommandWindow::~CommandWindow()
	{
		resetColors();
	}

	int CommandWindow::onNotify(unsigned int code)
	{
#if TESTING_WM_NOTIFY
		std::map<unsigned int, std::string> test;
		test[NM_OUTOFMEMORY] = "NM_OUTOFMEMORY";
		test[NM_CLICK] = "NM_CLICK";
		test[NM_DBLCLK] = "NM_DBLCLK";
		test[NM_RETURN] = "NM_RETURN";
		test[NM_RCLICK] = "NM_RCLICK";
		test[NM_RDBLCLK] = "NM_RDBLCLK";
		test[NM_SETFOCUS] = "NM_SETFOCUS";
		test[NM_KILLFOCUS] = "NM_KILLFOCUS";
		test[NM_CUSTOMDRAW] = "NM_CUSTOMDRAW";
		test[NM_HOVER] = "NM_HOVER";
		test[NM_NCHITTEST] = "NM_NCHITTEST";
		test[NM_KEYDOWN] = "NM_KEYDOWN";
		test[NM_RELEASEDCAPTURE] = "NM_RELEASEDCAPTURE";
		test[NM_SETCURSOR] = "NM_SETCURSOR";
		test[NM_CHAR] = "NM_CHAR";
		test[NM_TOOLTIPSCREATED] = "NM_TOOLTIPSCREATED";
		test[NM_LDOWN] = "NM_LDOWN";
		test[NM_RDOWN] = "NM_RDOWN";
		test[NM_THEMECHANGED] = "NM_THEMECHANGED";
		test[BN_SETFOCUS] = "BN_SETFOCUS ";
		std::cout << "onNotify: " << code << " " << (int)code << " " << test[code] << std::endl;
#endif

		auto fun = m_onNotifyCb.find(code);
		if (fun != m_onNotifyCb.end()) {
			fun->second();
		}
		return 0;
	}

	void CommandWindow::setOnNotifyCb(unsigned int code, const std::function<void()>& fun)
	{
		if (fun) {
			m_onNotifyCb[code] = fun;
		}
		else {
			m_onNotifyCb.erase(code);
		}
	}

	void CommandWindow::setOnChange(const std::function<void()>& fun) { assert(!"not Supported"); }
	void CommandWindow::setOnOutOfMemory(const std::function<void()>& fun) { setOnNotifyCb((unsigned)NM_OUTOFMEMORY, fun); }
	void CommandWindow::setOnClick(const std::function<void()>& fun) { setOnNotifyCb((unsigned)NM_CLICK, fun); }
	void CommandWindow::setOnBbClick(const std::function<void()>& fun) { setOnNotifyCb((unsigned)NM_DBLCLK, fun); }
	void CommandWindow::setOnReturn(const std::function<void()>& fun) { setOnNotifyCb((unsigned)NM_RETURN, fun); }
	void CommandWindow::setOnRClick(const std::function<void()>& fun) { setOnNotifyCb((unsigned)NM_RCLICK, fun); }
	void CommandWindow::setOnRDbClick(const std::function<void()>& fun) { setOnNotifyCb((unsigned)NM_RDBLCLK, fun); }
	void CommandWindow::setOnSetFocus(const std::function<void()>& fun) { setOnNotifyCb((unsigned)NM_SETFOCUS, fun); }
	void CommandWindow::setOnKillFocus(const std::function<void()>& fun) { setOnNotifyCb((unsigned)NM_KILLFOCUS, fun); }
	void CommandWindow::setOnCustomDraw(const std::function<void()>& fun) { setOnNotifyCb((unsigned)NM_CUSTOMDRAW, fun); }
	void CommandWindow::setOnHover(const std::function<void()>& fun) { setOnNotifyCb((unsigned)NM_HOVER, fun); }
	void CommandWindow::setOnNcHitTest(const std::function<void()>& fun) { setOnNotifyCb((unsigned)NM_NCHITTEST, fun); }
	void CommandWindow::setOnKeyDown(const std::function<void()>& fun) { setOnNotifyCb((unsigned)NM_KEYDOWN, fun); }
	void CommandWindow::setOnReleasedCapture(const std::function<void()>& fun) { setOnNotifyCb((unsigned)NM_RELEASEDCAPTURE, fun); }
	void CommandWindow::setOnSetCursor(const std::function<void()>& fun) { setOnNotifyCb((unsigned)NM_SETCURSOR, fun); }
	void CommandWindow::setOnChar(const std::function<void()>& fun) { setOnNotifyCb((unsigned)NM_CHAR, fun); }
	void CommandWindow::setOnTooltipsCreated(const std::function<void()>& fun) { setOnNotifyCb((unsigned)NM_TOOLTIPSCREATED, fun); }
	void CommandWindow::setOnLDown(const std::function<void()>& fun) { setOnNotifyCb((unsigned)NM_LDOWN, fun); }
	void CommandWindow::setOnRDown(const std::function<void()>& fun) { setOnNotifyCb((unsigned)NM_RDOWN, fun); }
	void CommandWindow::setOnThemeChanged(const std::function<void()>& fun) { setOnNotifyCb((unsigned)NM_THEMECHANGED, fun); }

	void CommandWindow::setBackground(COLORREF color)
	{
		if (m_backgroundBrush)
			::DeleteObject(m_backgroundBrush);
		m_backgroundBrush = CreateSolidBrush(color);
	}

	void CommandWindow::resetColors()
	{
		if (m_backgroundBrush)
			::DeleteObject(m_backgroundBrush);
		m_backgroundBrush = NULL;

		m_backColor.reset();
		m_textColor.reset();
	}

}
