#include <memory>
#include <vector>
#include <string>
#include <functional>
#include "platform.h"

#include "easywin-menu.h"

#include "easywin-utf8.h"

namespace easywin
{
	int Menu::getNextId()
	{
		static int nextId = 1;
		return nextId++;
	}

	void Menu::notifyListeners()
	{
		for (auto& l : m_listeners) {
			if (!l.expired()) {
				l.lock()->onMenuUpdate(shared_from_this());
			}
		}
	}

	Menu::Ptr Menu::Create()
	{
		auto menu = Menu::Ptr(new Menu);

		menu->m_hmenu = CreateMenu();

		return menu;
	}

	Menu::~Menu()
	{
		if (m_hmenu) {
			DestroyMenu(m_hmenu);
			m_hmenu = NULL;
		}
	}

	Menu::Ptr Menu::addSubMenu(const std::wstring& name)
	{
		auto submenu = Menu::Create();
		AppendMenuW(m_hmenu, MF_STRING | MF_POPUP, (UINT_PTR)submenu->getHMENU(), name.c_str());
		m_submenus.emplace_back(submenu);
		notifyListeners();
		return submenu;
	}

	int Menu::addMenuItem(const std::wstring& name, std::function<void()> command)
	{
		int id = getNextId();
		AppendMenuW(m_hmenu, MF_STRING, id, name.c_str());
		m_commands[id] = command;
		notifyListeners();
		return id;
	}

	int Menu::addMenuItem(const std::wstring& name, const std::wstring& shortcut, std::function<void()> command)
	{
		if (shortcut.empty())
			return addMenuItem(name, command);
		auto text = name + L"\t" + shortcut;
		return addMenuItem(text, command);
	}

	void Menu::addSeparator()
	{
		AppendMenuW(m_hmenu, MF_SEPARATOR, 0, NULL);
		notifyListeners();
	}

	void Menu::registerListener(MenuChangeListener::Ptr listener)
	{
		m_listeners.emplace_back(listener);
		notifyListeners();
	}

	void Menu::deregisterListener(MenuChangeListener::Ptr listener)
	{
		m_listeners.erase(std::remove_if(m_listeners.begin(), m_listeners.end(),
			[listener](MenuChangeListener::WPtr x) {
				return x.expired() || x.lock() == listener;
			}),
			m_listeners.end());
	}

	void Menu::onMenuUpdate(Menu::Ptr menu)
	{
		notifyListeners();
	}

	void Menu::onCommand(int id)
	{
		auto fun = m_commands.find(id);
		if (fun != m_commands.end()) {
			fun->second();
		}
		else {
			for (auto& s : m_submenus)
				s->onCommand(id);
		}
	}

	void Menu::setItemEnabled(int id, bool enabled)
	{
		::EnableMenuItem(m_hmenu, id, MF_BYCOMMAND | (enabled ? MF_ENABLED : MF_DISABLED));
	}

	void Menu::setItemChecked(int id, bool checked)
	{
		::CheckMenuItem(m_hmenu, id, MF_BYCOMMAND | (checked ? MF_CHECKED : MF_UNCHECKED));
	}

	void Menu::setText(int id, const std::wstring& text, const std::wstring& shortcut)
	{
		auto name = text;
		if (!shortcut.empty())
			name = name + L"\t" + shortcut;
		MENUITEMINFOW info = {};
		info.cbSize = sizeof(info);
		info.fMask = MIIM_STRING;
		info.dwTypeData = (LPWSTR)name.c_str();
		::SetMenuItemInfoW(m_hmenu, id, FALSE, &info);
	}

	void Menu::setName(int id, const std::wstring& name)
	{
		setText(id, name, getShortcut(id));
	}

	void Menu::setShortcut(int id, const std::wstring& shortcut)
	{
		setText(id, getName(id), shortcut);
	}

	std::wstring Menu::getText(int id)
	{
		MENUITEMINFOW info = {};
		info.cbSize = sizeof(info);
		info.fMask = MIIM_STRING;
		::GetMenuItemInfo(m_hmenu, id, FALSE, &info);
		info.cch++;
		std::wstring buff(info.cch, 0);
		info.dwTypeData = (LPWSTR)buff.data();
		::GetMenuItemInfo(m_hmenu, id, FALSE, &info);
		return buff;
	}

	std::wstring Menu::getName(int id)
	{
		auto text = getText(id);
		auto pos = text.find_first_of('\t');
		if (pos == std::wstring::npos) {
			return text;
		}
		auto name = text.substr(0, pos);
		return name;
	}

	std::wstring Menu::getShortcut(int id)
	{
		auto text = getText(id);
		auto pos = text.find_first_of('\t');
		if (pos == std::wstring::npos) {
			return L"";
		}
		auto shortcut = text.substr(pos + 1);
		return shortcut;
	}
}