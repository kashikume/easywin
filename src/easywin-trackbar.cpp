#include "platform.h"
#include "types.h"

#include "easywin-trackbar.h"

#include "easywin-windowbase.h"
#include "easywin-window.h"

namespace easywin
{
	Trackbar::Ptr Trackbar::Create(Window::Ptr& parent, const Rect& rect, bool vertical)
	{
		auto trackbar = Trackbar::Ptr(new Trackbar);

		::InitCommonControls(); // loads common control's DLL 

		trackbar->WindowBase::createWindow(
			TRACKBAR_CLASSW,
			L"",
			WS_CHILD |
			WS_VISIBLE |
			(vertical ? TBS_VERT : TBS_HORZ) |
			TBS_AUTOTICKS,
			rect,
			parent);
		return trackbar;
	}

	void Trackbar::setRange(int rmin, int rmax)
	{
		SendMessage(m_hwnd, TBM_SETRANGE,
			(WPARAM)TRUE,                   // redraw flag 
			(LPARAM)MAKELONG(rmin, rmax));  // min. & max. positions
	}

	void Trackbar::setPageSize(int page)
	{
		SendMessage(m_hwnd, TBM_SETPAGESIZE, 0, (LPARAM)page);
	}
	void Trackbar::setPos(int pos)
	{
		SendMessage(m_hwnd, TBM_SETPOS,
			(WPARAM)TRUE,                   // redraw flag 
			(LPARAM)pos);
	}

	void Trackbar::setTicFreq(int freq)
	{
		SendMessage(m_hwnd, TBM_SETTICFREQ, freq, 0);
	}

	int Trackbar::getPos()
	{
		return (int)SendMessage(m_hwnd, TBM_GETPOS, 0, 0);
	}

	int Trackbar::callOnScroll(short mode, short pos)
	{
		callOnCommand(0);
		return 0;
	}

}