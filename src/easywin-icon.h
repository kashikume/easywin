#pragma once

#include <memory>

#include "types.h"
#include "platform.h"

namespace easywin
{
	class Icon
	{
	public:
		typedef std::shared_ptr<Icon> Ptr;

		static Ptr CreateSystemIcon(LPWSTR id);
		static Ptr CreateResIcon(int id);
		static Ptr CreateFromTransparentBitmap(const Bitmap_Ptr& bitmap);

		void draw(const PaintDC_Ptr& dc, int x, int y);
		void draw(PaintDC& dc, int x, int y);

		HICON handle() const { return m_hicon; }

		~Icon();
	private:
		HICON m_hicon = NULL;
		bool m_requiresRemoval = false;
	};
}