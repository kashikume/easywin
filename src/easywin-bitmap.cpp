#include <compare>
#include <memory>
#include <filesystem>
#include <vector>

#include "platform.h"
#include "types.h"

#include "easywin-bitmap.h"

#include "easywin-bitmaptools.h"
#include "easywin-paintdc.h"

namespace easywin
{

	Bitmap::Bitmap()
	{

	}

	Bitmap::~Bitmap()
	{
		DeleteObject(m_hbitmap);
		DeleteObject(m_hMask);
	}

	Bitmap::Ptr Bitmap::CreateCompatibile(PaintDC& dc, int width, int height)
	{
		auto bmp = std::shared_ptr<Bitmap>(new Bitmap());
		bmp->m_hbitmap = (HBITMAP)CreateCompatibleBitmap(dc.getHdc(), width, height);
		GetObject(bmp->m_hbitmap, sizeof(BITMAP), &bmp->m_bitmapInfo);
		return bmp;
	}

	Bitmap::Ptr Bitmap::CreateCompatibile(PaintDC_Ptr& dc, int width, int height)
	{
		auto bmp = std::shared_ptr<Bitmap>(new Bitmap());
		bmp->m_hbitmap = (HBITMAP)CreateCompatibleBitmap(dc->getHdc(), width, height);
		GetObject(bmp->m_hbitmap, sizeof(BITMAP), &bmp->m_bitmapInfo);
		return bmp;
	}

	Bitmap::Ptr Bitmap::CreateFromBmpFile(const std::filesystem::path& filename)
	{
		auto bmp = std::shared_ptr<Bitmap>(new Bitmap());
		bmp->m_hbitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), filename.generic_wstring().c_str(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		GetObject(bmp->m_hbitmap, sizeof(BITMAP), &bmp->m_bitmapInfo);
		return bmp;
	}

	Bitmap::Ptr Bitmap::CreateFromRGBAData(size_t w, size_t h, const std::vector<unsigned char>& pixels24bit)
	{
		auto bmp = Bitmap::Ptr(new Bitmap());
		bmp->m_hbitmap = (HBITMAP)CreateBitmap((int)w, (int)h, 1, 32, pixels24bit.data());
		GetObject(bmp->m_hbitmap, sizeof(BITMAP), &bmp->m_bitmapInfo);
		return bmp;
	}

	Bitmap::Ptr Bitmap::CreateFromRGBAData(size_t w, size_t h, const std::vector<unsigned>& pixels24bit)
	{
		auto bmp = Bitmap::Ptr(new Bitmap());
		bmp->m_hbitmap = (HBITMAP)CreateBitmap((int)w, (int)h, 1, 32, pixels24bit.data());
		GetObject(bmp->m_hbitmap, sizeof(BITMAP), &bmp->m_bitmapInfo);
		return bmp;
	}


	Bitmap::Ptr Bitmap::CreateEmpty(size_t w, size_t h)
	{
		std::vector<unsigned char> data(w * h * 4L, 0); // no need for alighment
		return CreateFromRGBAData(w, h, data);
	}

	std::vector<unsigned char> Bitmap::getRGBA()
	{
		std::vector<unsigned char> output((size_t)width() * (size_t)height() * 4L);

		BITMAPINFO bmi = {};
		bmi.bmiHeader.biSize = sizeof(bmi.bmiHeader);
		bmi.bmiHeader.biWidth = width();
		bmi.bmiHeader.biHeight = -height();

		bmi.bmiHeader.biPlanes = 1;
		bmi.bmiHeader.biBitCount = 32;
		bmi.bmiHeader.biCompression = BI_RGB;
		bmi.bmiHeader.biSizeImage = 0;
		bmi.bmiHeader.biXPelsPerMeter = 0;
		bmi.bmiHeader.biYPelsPerMeter = 0;
		bmi.bmiHeader.biClrUsed = 0;
		bmi.bmiHeader.biClrImportant = 0;

		::GetDIBits(
			getPaintDC()->getHdc(),
			m_hbitmap,
			0,
			height(),
			output.data(),
			&bmi,
			DIB_RGB_COLORS
		);

		return output;
	}

	Bitmap::Ptr Bitmap::getScalledCopy(float scale)
	{
		int destWidth = (int)(width() * scale);
		int destHeight = (int)(height() * scale);
		return getScalledCopy(destWidth, destHeight);
	}

	Bitmap::Ptr Bitmap::getScalledCopy(size_t w, size_t h)
	{
		auto data = getRGBA();
		BitmapTools tools(width(), height(), data);
		auto out = tools.scaleTo(w, h);
		return CreateFromRGBAData(w, h, out);
	}

	Bitmap::Ptr Bitmap::crop(const Rect& rect)
	{
		auto out = Bitmap::CreateEmpty(rect.width(), rect.height());
		auto dc = out->getPaintDC();
		dc->drawBitmap(-rect.left, -rect.top, *this);
		return out;
	}

	PaintDC_Ptr Bitmap::getPaintDC(PaintDC& baseDC)
	{
		if (m_bitmapDC.expired()) {
			auto dc = baseDC.createCompatibile(*this);
			m_bitmapDC = dc;
			return m_bitmapDC.lock();
		}
		else {
			return m_bitmapDC.lock();
		}
	}

	PaintDC_Ptr Bitmap::getPaintDC(PaintDC_Ptr& baseDC)
	{
		if (m_bitmapDC.expired()) {
			auto dc = baseDC->createCompatibile(*this);
			m_bitmapDC = dc;
			return m_bitmapDC.lock();
		}
		else {
			return m_bitmapDC.lock();
		}
	}

	PaintDC_Ptr Bitmap::getPaintDC()
	{
		if (m_bitmapDC.expired()) {
			PaintDC_Ptr baseDC = PaintDC_Ptr(new PaintDC((HDC)NULL));
			auto dc = baseDC->createCompatibile(*this);
			m_bitmapDC = dc;
			return m_bitmapDC.lock();
		}
		else {
			return m_bitmapDC.lock();
		}
	}

	void Bitmap::setTransparency(COLORREF bkColor)
	{
		if (m_hMask) {
			DeleteObject(m_hMask);
			m_hMask = NULL;
		}

		HDC hdcMem, hdcMem2;
		BITMAP bm;

		GetObject(m_hbitmap, sizeof(BITMAP), &bm);
		m_hMask = CreateBitmap(bm.bmWidth, bm.bmHeight, 1, 1, NULL);

		hdcMem = CreateCompatibleDC(0);
		hdcMem2 = CreateCompatibleDC(0);

		SelectObject(hdcMem, m_hbitmap);
		SelectObject(hdcMem2, m_hMask);
		SetBkColor(hdcMem, bkColor);
		BitBlt(hdcMem2, 0, 0, bm.bmWidth, bm.bmHeight, hdcMem, 0, 0, SRCCOPY);
		BitBlt(hdcMem, 0, 0, bm.bmWidth, bm.bmHeight, hdcMem2, 0, 0, SRCINVERT);
		DeleteDC(hdcMem);
		DeleteDC(hdcMem2);
	}

	void Bitmap::draw(PaintDC& dc, int x, int y)
	{
		auto bitmapDC = getPaintDC(dc);
		if (m_hMask) {
			SelectObject(bitmapDC->getHdc(), m_hMask);
			BitBlt(dc.getHdc(), x, y, m_bitmapInfo.bmWidth, m_bitmapInfo.bmHeight, bitmapDC->getHdc(), 0, 0, SRCAND);

			SelectObject(bitmapDC->getHdc(), m_hbitmap);
			BitBlt(dc.getHdc(), x, y, m_bitmapInfo.bmWidth, m_bitmapInfo.bmHeight, bitmapDC->getHdc(), 0, 0, SRCPAINT);
		}
		else {
			BitBlt(dc.getHdc(), x, y, m_bitmapInfo.bmWidth, m_bitmapInfo.bmHeight, bitmapDC->getHdc(), 0, 0, SRCCOPY);
		}
	}

	void Bitmap::draw(PaintDC& dc, const Rect& dstRc)
	{
		if (dstRc.width() == m_bitmapInfo.bmWidth && dstRc.height() == m_bitmapInfo.bmHeight) {
			draw(dc, dstRc.left, dstRc.top);
			return;
		}

		auto bitmapDC = getPaintDC(dc);
		if (m_hMask) {
			::SelectObject(bitmapDC->getHdc(), m_hMask);
			::StretchBlt(dc.getHdc(), dstRc.left, dstRc.top, dstRc.width(), dstRc.height(), bitmapDC->getHdc(), 0, 0, m_bitmapInfo.bmWidth, m_bitmapInfo.bmHeight, SRCAND);

			::SelectObject(bitmapDC->getHdc(), m_hbitmap);
			::StretchBlt(dc.getHdc(), dstRc.left, dstRc.top, dstRc.width(), dstRc.height(), bitmapDC->getHdc(), 0, 0, m_bitmapInfo.bmWidth, m_bitmapInfo.bmHeight, SRCPAINT);
		}
		else {
			::StretchBlt(dc.getHdc(), dstRc.left, dstRc.top, dstRc.width(), dstRc.height(), bitmapDC->getHdc(), 0, 0, m_bitmapInfo.bmWidth, m_bitmapInfo.bmHeight, SRCCOPY);
		}
	}

	void Bitmap::alphaBlend(PaintDC& dc, int x, int y, int alpha)
	{
		auto bitmapDC = getPaintDC(dc);
		BLENDFUNCTION bStruct;
		bStruct.BlendOp = AC_SRC_OVER;
		bStruct.BlendFlags = 0;
		bStruct.SourceConstantAlpha = alpha;
		bStruct.AlphaFormat = AC_SRC_ALPHA;
		AlphaBlend(dc.getHdc(), x, y, m_bitmapInfo.bmWidth, m_bitmapInfo.bmHeight, bitmapDC->getHdc(), 0, 0, m_bitmapInfo.bmWidth, m_bitmapInfo.bmHeight, bStruct);
	}

	void Bitmap::alphaBlend(PaintDC& dc, const Rect& dstRc, int alpha)
	{
		auto bitmapDC = getPaintDC(dc);

		BLENDFUNCTION bStruct;
		bStruct.BlendOp = AC_SRC_OVER;
		bStruct.BlendFlags = 0;
		bStruct.SourceConstantAlpha = alpha;
		bStruct.AlphaFormat = AC_SRC_ALPHA;
		AlphaBlend(dc.getHdc(), dstRc.left, dstRc.top, dstRc.width(), dstRc.height(), bitmapDC->getHdc(), 0, 0, m_bitmapInfo.bmWidth, m_bitmapInfo.bmHeight, bStruct);
	}

}