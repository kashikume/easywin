#include <sstream>
#include <functional>

#include "types.h"
#include "platform.h"

#include "easywin-window.h"

#include "easywin-app.h"
#include "easywin-commandwindow.h"
#include "easywin-cursor.h"
#include "easywin-icon.h"
#include "easywin-menu.h"
#include "easywin-scrollbar.h"
#include "easywin-trackbar.h"
#include "easywin-utf8.h"

namespace easywin
{
	Window::Window()
	{

	}

	void Window::onMenuUpdate(SPMenu menu)
	{
		DrawMenuBar(m_hwnd);
	}

	void Window::registerDefaultMessages()
	{
		setOnMessage(WM_COMMAND, [this](UINT, WPARAM wParam, LPARAM lParam) {
			return onCommand(wParam, lParam);
			});

		setOnMessage(WM_NOTIFY, [this](UINT, WPARAM wParam, LPARAM lParam) {
			return onNotify(wParam, lParam);
			});

		setOnMessage(WM_HSCROLL, [this](UINT, WPARAM wParam, LPARAM lParam) {
			return onHScroll(wParam, lParam);
			});

		setOnMessage(WM_VSCROLL, [this](UINT, WPARAM wParam, LPARAM lParam) {
			return onVScroll(wParam, lParam);
			});

		setOnMessage(WM_CTLCOLORSTATIC, [this](UINT msg, WPARAM wParam, LPARAM lParam) {
			return onCtrlColor(msg, wParam, lParam);
			});

		setOnMessage(WM_CTLCOLORSCROLLBAR, [this](UINT msg, WPARAM wParam, LPARAM lParam) {
			return onCtrlColor(msg, wParam, lParam);
			});

		setOnMessage(WM_CTLCOLOREDIT, [this](UINT msg, WPARAM wParam, LPARAM lParam) {
			return onCtrlColor(msg, wParam, lParam);
			});

		setOnMessage(WM_SETCURSOR, [this](UINT msg, WPARAM wParam, LPARAM lParam) -> LRESULT {
			if (!onSetCursor()) {
				return 0;
			}
			else {
				return ::DefWindowProcW(m_hwnd, msg, wParam, lParam);
			}
			});

		setOnMessage(WM_GETMINMAXINFO, std::bind(&Window::onGetMinMaxInfo, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
		setonDpiChanged(std::bind(&Window::onDpiChangedProc, this, std::placeholders::_1, std::placeholders::_2));
	}

	void Window::setupPixelFormatGL(bool doubleBuffer)
	{
		HDC hDC = GetDC(m_hwnd);

		PIXELFORMATDESCRIPTOR pfd{};
		pfd.nSize = sizeof(pfd);
		pfd.nVersion = 1;
		pfd.dwFlags =
			PFD_DRAW_TO_WINDOW
			| PFD_SUPPORT_OPENGL
			| PFD_GENERIC_ACCELERATED
			| PFD_GENERIC_FORMAT
			| (doubleBuffer ? PFD_SWAP_EXCHANGE : 0)
			| (doubleBuffer ? PFD_DOUBLEBUFFER : 0);
		pfd.dwLayerMask = PFD_MAIN_PLANE;
		pfd.iPixelType = PFD_TYPE_RGBA;
		pfd.cColorBits = 32;
		pfd.cDepthBits = 24;
		pfd.cStencilBits = 8;

		int pf = ChoosePixelFormat(hDC, &pfd);
		SetPixelFormat(hDC, pf, &pfd);
		DescribePixelFormat(hDC, pf, sizeof(PIXELFORMATDESCRIPTOR), &pfd);

		ReleaseDC(m_hwnd, hDC);
	}

	Window::Ptr Window::CreateTopWindow(bool verticalScroll, bool horisontalScroll)
	{
		auto window = Window::Ptr(new Window);
		window->registerClass();

		window->WindowBase::createWindow(
			window->m_windowClassName,
			L"",
			WS_OVERLAPPEDWINDOW | (verticalScroll ? WS_VSCROLL : 0) | (horisontalScroll ? WS_HSCROLL : 0) | WS_CLIPCHILDREN,
			RECT{ CW_USEDEFAULT, 0, CW_USEDEFAULT, 0 },
			nullptr);

		window->registerDefaultMessages();

		return window;
	}

	Window::Ptr Window::CreateClient(WindowBase::Ptr parent, const RECT& rect, bool verticalScroll, bool horisontalScroll)
	{
		auto window = std::shared_ptr<Window>(new Window);
		window->registerClass();

		window->WindowBase::createWindow(
			window->m_windowClassName,
			L"",
			WS_CHILD | WS_CLIPCHILDREN | WS_VISIBLE | (verticalScroll ? WS_VSCROLL : 0) | (horisontalScroll ? WS_HSCROLL : 0),
			rect,
			parent);

		window->registerDefaultMessages();

		return window;
	}

	Window::Ptr Window::CreateFullScreenWindowGL(SetPixelFormatFunction setPxFormat, bool doubleBuffer)
	{
		auto window = std::shared_ptr<Window>(new Window);
		window->registerClassGL();

		HMONITOR hmon = ::MonitorFromWindow(nullptr, MONITOR_DEFAULTTONEAREST);
		MONITORINFO mi = { sizeof(mi) };
		if (!GetMonitorInfo(hmon, &mi)) return NULL;

		window->WindowBase::createWindow(
			window->m_windowClassName,
			L"",
			WS_POPUP | WS_VISIBLE,
			mi.rcMonitor,
			nullptr);

		bool pixelFormatSet = false;
		if (setPxFormat) {
			pixelFormatSet = setPxFormat(window, doubleBuffer);
		}
		if (!pixelFormatSet) {
			window->setupPixelFormatGL(doubleBuffer);
		}

		window->registerDefaultMessages();

		return window;
	}

	Window::Ptr Window::CreateTopWindowGL(SetPixelFormatFunction setPxFormat, bool doubleBuffer, bool verticalScroll, bool horisontalScroll)
	{
		auto window = std::shared_ptr<Window>(new Window);
		window->registerClassGL();

		window->WindowBase::createWindow(
			window->m_windowClassName,
			L"",
			WS_OVERLAPPEDWINDOW | (verticalScroll ? WS_VSCROLL : 0) | (horisontalScroll ? WS_HSCROLL : 0),
			RECT{ CW_USEDEFAULT, 0, CW_USEDEFAULT, 0 },
			nullptr);

		bool pixelFormatSet = false;
		if (setPxFormat) {
			pixelFormatSet = setPxFormat(window, doubleBuffer);
		}
		if (!pixelFormatSet) {
			window->setupPixelFormatGL(doubleBuffer);
		}

		window->registerDefaultMessages();

		return window;
	}

	Window::Ptr Window::CreateClientGL(WindowBase::Ptr parent, const RECT& rect, SetPixelFormatFunction setPxFormat, bool doubleBuffer, bool verticalScroll, bool horisontalScroll)
	{
		auto window = std::shared_ptr<Window>(new Window);
		window->registerClassGL();

		window->WindowBase::createWindow(
			window->m_windowClassName,
			L"",
			WS_CHILD | WS_CLIPCHILDREN | WS_VISIBLE | (verticalScroll ? WS_VSCROLL : 0) | (horisontalScroll ? WS_HSCROLL : 0),
			rect,
			parent);

		bool pixelFormatSet = false;
		if (setPxFormat) {
			pixelFormatSet = setPxFormat(window, doubleBuffer);
		}
		if (!pixelFormatSet) {
			window->setupPixelFormatGL(doubleBuffer);
		}

		window->registerDefaultMessages();

		return window;
	}

	Window::Ptr Window::CreateModalWindow(Window::Ptr parent, int w, int h, const std::function<void(Window::Ptr)>& createFun, bool verticalScroll, bool horisontalScroll)
	{
		auto window = std::shared_ptr<Window>(new Window);
		window->registerClass();

		auto parentRc = parent->getWindowRect();
		int x = parentRc.left + (parentRc.width() - w) / 2;
		int y = parentRc.top + (parentRc.height() - h) / 3;

		window->WindowBase::createWindow(
			window->m_windowClassName,
			L"",
			WS_OVERLAPPEDWINDOW | (verticalScroll ? WS_VSCROLL : 0) | (horisontalScroll ? WS_HSCROLL : 0) | WS_CLIPCHILDREN,
			RECT{ CW_USEDEFAULT, 0, CW_USEDEFAULT, 0 },
			parent);

		window->registerDefaultMessages();

		window->moveWindow(RECT{ x, y, x + w, y + h });

		if (createFun) {
			createFun(window);
		}

		window->m_beforeDestroy = [parent]() {
			parent->enable();
			parent->setFocus();
		};

		parent->disable();

		window->update();
		window->show();

		return window;
	}


	bool Window::s_idleEnabled = false;

	void Window::runMessageLoop()
	{
		MSG msg;
		while (GetMessage(&msg, nullptr, 0, 0)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	void Window::runMessageLoop(const std::function<void()>& idleFunction)
	{
		MSG msg = {};

		s_idleEnabled = true;

		for (;;) {
			if (s_idleEnabled) {
				while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) > 0) {
					TranslateMessage(&msg);
					DispatchMessage(&msg);
					if (msg.message == WM_QUIT) return;
				}
				idleFunction();
			}
			else {
				for (;;) {
					bool exit = !GetMessage(&msg, nullptr, 0, 0);
					if (exit) return;
					TranslateMessage(&msg);
					DispatchMessage(&msg);
					if (s_idleEnabled) break;
				}
			}
		}
	}

	void Window::enableIdle()
	{
		s_idleEnabled = true;
		theApp::getInstance().pushMessageLoop();
	}

	void Window::coverFullScreen(const Window::Ptr& screen)
	{
		HWND screen_hwnd = screen ? screen->getHWnd() : NULL;
		HMONITOR hmon = ::MonitorFromWindow(screen_hwnd, MONITOR_DEFAULTTONEAREST);
		MONITORINFO mi = { sizeof(mi) };
		if (!GetMonitorInfo(hmon, &mi)) return;
		moveWindow(mi.rcMonitor);
	}

	void Window::close()
	{
		::SendMessage(m_hwnd, WM_CLOSE, 0, 0);
	}

	void Window::setOnEreaseBg(const std::function<LRESULT(PaintDC&)>& function)
	{
		setOnMessage(WM_ERASEBKGND, [=](UINT, WPARAM wParam, LPARAM)->LRESULT { PaintDC dc(m_hwnd, (HDC)wParam); return function(dc); });
	}

	void Window::setOnDraw(const std::function<LRESULT(PaintDC&)>& function)
	{
		setOnMessage(WM_PAINT, [=](UINT, WPARAM, LPARAM) -> LRESULT {
			PaintDC dc(getHWnd());
		return function(dc);
			});
	}

	void Window::setOnBufferedDraw(const std::function<void(PaintDC::Ptr&)>& function)
	{
		setOnDraw([this, function](PaintDC& dc) {
			Rect rc = getClientRect();
		if (!m_drawingBuffer || m_drawingBuffer->height() != rc.height() || m_drawingBuffer->width() != rc.width()) {
			m_drawingBuffer = Bitmap::CreateCompatibile(dc, rc.width(), rc.height());
		}
		auto bufferDC = m_drawingBuffer->getPaintDC(dc);
		function(bufferDC);
		dc.drawBitmap(0, 0, *m_drawingBuffer);
		return 0;
			});
		setOnEreaseBg([](PaintDC&) {return 1; });
	}

	PaintDC::Ptr Window::getDC()
	{
		return PaintDC::getFromWindow(getReference());
	}

	LRESULT Window::wndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
	{
		LRESULT res = 0;
		if (m_messageFunctions.count(message) == 1) {
			res = m_messageFunctions[message](message, wParam, lParam);
		}
		else {
			if (message == WM_CLOSE) {
				destroy();
			}
			res = DefWindowProc(hwnd, message, wParam, lParam);
		}
		if (message == WM_DESTROY) {
			if (m_beforeDestroy)
				m_beforeDestroy();
			removeSelf();
		}
		return res;
	}

	LRESULT Window::onGetMinMaxInfo(UINT, WPARAM, LPARAM lParam)
	{
		LPMINMAXINFO lpMMI = (LPMINMAXINFO)lParam;
		if (m_minWidth.has_value()) {
			lpMMI->ptMinTrackSize.x = m_minWidth.value();
		}
		if (m_minHeight.has_value()) {
			lpMMI->ptMinTrackSize.y = m_minHeight.value();
		}
		return 0;
	}

	void Window::setFeature(const Feature& feature, bool enable)
	{
		auto windowStyle = ::GetWindowLong(m_hwnd, GWL_STYLE);
		switch (feature)
		{
		case Feature::resizeFrame:
			::SetWindowLong(m_hwnd, GWL_STYLE, (windowStyle & ~WS_SIZEBOX) | (enable ? WS_SIZEBOX : 0));
			break;
		case Feature::minimizeBox:
			::SetWindowLong(m_hwnd, GWL_STYLE, (windowStyle & ~WS_MINIMIZEBOX) | (enable ? WS_MINIMIZEBOX : 0));
			break;
		case Feature::maximizeBox:
			::SetWindowLong(m_hwnd, GWL_STYLE, (windowStyle & ~WS_MAXIMIZEBOX) | (enable ? WS_MAXIMIZEBOX : 0));
			break;
		case Feature::sysMenu:
			::SetWindowLong(m_hwnd, GWL_STYLE, (windowStyle & ~WS_SYSMENU) | (enable ? WS_SYSMENU : 0));
			break;
		case Feature::caption:
			::SetWindowLong(m_hwnd, GWL_STYLE, (windowStyle & ~WS_CAPTION) | (enable ? WS_CAPTION : 0));
			break;

		}
	}

	int Window::s_nextWindowClassId = 1;

	void Window::registerClass()
	{
		HINSTANCE hInstance = GetModuleHandle(NULL);

		std::wstringstream ss;
		ss << L"easywin_" << s_nextWindowClassId++;
		m_windowClassName = ss.str();

		WNDCLASSEXW wcex = {};

		wcex.cbSize = sizeof(WNDCLASSEX);

		wcex.style = CS_HREDRAW | CS_VREDRAW;
		wcex.lpfnWndProc = [](HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)->LRESULT {
			auto win = theApp::getInstance().getWindow(hwnd);
			if (win)
			{
				return win->wndProc(hwnd, message, wParam, lParam);
			}
			else
			{
				return DefWindowProc(hwnd, message, wParam, lParam);
			}
		};
		wcex.cbClsExtra = 0;
		wcex.cbWndExtra = 0;
		wcex.hInstance = hInstance;
		wcex.hIcon = 0;
		wcex.hCursor = LoadCursorW(NULL, IDC_ARROW);
		wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW);
		wcex.lpszMenuName = 0;
		wcex.lpszClassName = m_windowClassName.c_str();
		wcex.hIconSm = 0;

		RegisterClassExW(&wcex);
	}

	void Window::registerClassGL()
	{
		HINSTANCE hInstance = GetModuleHandle(NULL);

		std::wstringstream ss;
		ss << L"easywin_gl_" << s_nextWindowClassId++;
		m_windowClassName = ss.str();

		WNDCLASSEXW wcex = {};

		wcex.cbSize = sizeof(WNDCLASSEX);

		wcex.style = CS_OWNDC;
		using namespace std::placeholders;
		wcex.lpfnWndProc = [](HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)->LRESULT {
			auto win = theApp::getInstance().getWindow(hwnd);
			if (win) {
				return win->wndProc(hwnd, message, wParam, lParam);
			}
			else {
				return DefWindowProc(hwnd, message, wParam, lParam);
			}
		};
		wcex.cbClsExtra = 0;
		wcex.cbWndExtra = 0;
		wcex.hInstance = hInstance;
		wcex.hIcon = 0;
		wcex.hCursor = LoadCursorW(NULL, IDC_ARROW);
		wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW);
		wcex.lpszMenuName = 0;
		wcex.lpszClassName = m_windowClassName.c_str();
		wcex.hIconSm = 0;

		RegisterClassExW(&wcex);
	}


	void Window::setOnSysKeyDown(const std::function<LRESULT(int)>& function)
	{
		setOnMessage(
			WM_SYSKEYDOWN,
			[=](UINT msg, WPARAM wParam, LPARAM lParam)->LRESULT {
				if (function((int)wParam)) {
					return ::DefWindowProc(getHWnd(), msg, wParam, lParam);
				}
				return 0;
			}
		);
	}

	void Window::setOnSysKeyUp(const std::function<LRESULT(WPARAM)>& function)
	{
		setOnMessage(
			WM_SYSKEYUP,
			[=](UINT msg, WPARAM wParam, LPARAM lParam)->LRESULT {
				if (function(wParam)) {
					return ::DefWindowProc(getHWnd(), msg, wParam, lParam);
				}
				return 0;
			}
		);
	}

	int Window::startTimer(int interval, const std::function<void()>& fun)
	{
		auto id = ++m_lastTimerId;
		m_timerFunctions[id] = fun;
		SetTimer(m_hwnd, id, interval, [](HWND hwnd,
			UINT Arg2,
			UINT_PTR Arg3,
			DWORD Arg4) {
				theApp::getInstance().getWindow(hwnd)->callTimerFunction((int)Arg3);
			});
		return id;
	}

	void Window::callTimerFunction(int id)
	{
		m_timerFunctions[id]();
	}

	void Window::stopTimer(int id)
	{
		KillTimer(m_hwnd, id);
		m_timerFunctions.erase(id);
	}

	int Window::callDelayed(int delay, const std::function<void()>& fun)
	{
		auto id = ++m_lastTimerId;
		m_timerFunctions[id] = [id, fun, this]() {
			KillTimer(m_hwnd, id);
			fun();
			m_timerFunctions.erase(id);
		};
		SetTimer(m_hwnd, id, delay, [](HWND hwnd,
			UINT Arg2,
			UINT_PTR Arg3,
			DWORD Arg4) {
				theApp::getInstance().getWindow(hwnd)->callTimerFunction((int)Arg3);
			});
		return id;
	}

	LRESULT Window::onCommand(WPARAM wParam, LPARAM lParam)
	{
		if (lParam == 0) {
			if (HIWORD(wParam) == 0) { // message comes from menu
				if (m_menu) {
					m_menu->onCommand(LOWORD(wParam));
				}
			}
		}
		else {
			for (auto& child : m_children) {
				if (child->getHWnd() == (HWND)lParam) {
					auto cw = std::dynamic_pointer_cast<CommandWindow>(child);
					if (cw) return cw->callOnCommand(HIWORD(wParam));
				}
			}
		}
		return 1;
	}

	void Window::setOnClose(const std::function < void() >& function)
	{
		setOnMessage(WM_CLOSE, [=](UINT, WPARAM, LPARAM)->LRESULT { function(); return 0; });
	}

	void Window::setOnDestroy(const std::function < void() >& function)
	{
		setOnMessage(WM_DESTROY, [=](UINT, WPARAM, LPARAM)->LRESULT { function(); return 0; });
	}


	void Window::setOnMove(const std::function<void(int x, int y)>& function)
	{
		setOnMessage(WM_MOVE, [=](UINT, WPARAM, LPARAM lParam)->LRESULT
			{
				function(LOWORD(lParam), HIWORD(lParam));
		return 0;
			});
	}

	void Window::setOnSize(const std::function< void(int width, int height, WindowShowStyle mode) >& function)
	{
		setOnMessage(WM_SIZE, [=](UINT, WPARAM wParam, LPARAM lParam)->LRESULT
			{
				WindowShowStyle style = WindowShowStyle::normal;
		switch (wParam)
		{
		case SIZE_MAXHIDE:
		case SIZE_MAXSHOW:
		case SIZE_RESTORED:
			style = WindowShowStyle::normal;
			break;
		case SIZE_MAXIMIZED:
			style = WindowShowStyle::maximized;
			break;
		case SIZE_MINIMIZED:
			style = WindowShowStyle::minimized;
			break;
		}
		function(LOWORD(lParam), HIWORD(lParam), style);
		return 0;
			});
	}

	LRESULT Window::onCtrlColor(UINT message, WPARAM wParam, LPARAM lParam)
	{
		for (auto& child : m_children) {
			if (child->getHWnd() == (HWND)lParam) {
				auto cw = std::dynamic_pointer_cast<CommandWindow>(child);
				if (cw) {
					auto ret = cw->getBackgroundBrush();

					HDC hdcStatic = (HDC)wParam;

					if (cw->getBackgroundColor().has_value())
						SetBkColor(hdcStatic, cw->getBackgroundColor().value());

					if (cw->getTextColor().has_value())
						SetTextColor(hdcStatic, cw->getTextColor().value());

					if (ret)
						return (LRESULT)ret;
				}
			}
		}
		return ::DefWindowProc(m_hwnd, message, wParam, lParam);
	}

	LRESULT Window::onNotify(WPARAM wParam, LPARAM lParam)
	{
		NMHDR* nmhdr = (NMHDR*)lParam;

		for (auto& child : m_children) {
			if (child->getHWnd() == (HWND)nmhdr->hwndFrom) {
				auto cw = std::dynamic_pointer_cast<CommandWindow>(child);
				if (cw) return cw->onNotify(nmhdr->code);
			}
		}
		return 1;
	}

	LRESULT Window::onVScroll(WPARAM wParam, LPARAM lParam)
	{

		if (lParam)
		{
			for (auto& child : m_children) {
				if (child->getHWnd() == (HWND)lParam) {
					auto sb = std::dynamic_pointer_cast<ScrollBar>(child);
					if (sb) {
						return sb->callOnScroll(LOWORD(wParam), HIWORD(wParam));
					}
					auto tb = std::dynamic_pointer_cast<Trackbar>(child);
					if (tb) {
						return tb->callOnScroll(LOWORD(wParam), HIWORD(wParam));
					}
				}
			}
		}
		else {
			return m_onVScrollCb ? m_onVScrollCb(LOWORD(wParam), HIWORD(wParam)) : defaultOnScroll(true, LOWORD(wParam), HIWORD(wParam));
		}
		return 1;
	}

	LRESULT Window::onSetCursor()
	{
		if (!m_cursor) return 1;
		m_cursor->set();
		return 0;
	}

	void Window::setonDpiChanged(const std::function<LRESULT(int dpi, const Rect& rc)>& function)
	{
		setOnMessage(WM_DPICHANGED,
			[=](UINT, WPARAM wParam, LPARAM lParam)->LRESULT {
				return function(LOWORD(wParam), Rect{ *((RECT*)lParam) });
			}
		);
	}

	LRESULT Window::onDpiChangedProc(int dpi, const Rect& rc)
	{
		moveWindow(rc);
		return 0;
	}

	LRESULT Window::onHScroll(WPARAM wParam, LPARAM lParam)
	{
		if (lParam)
		{
			for (auto& child : m_children) {
				if (child->getHWnd() == (HWND)lParam) {
					auto sb = std::dynamic_pointer_cast<ScrollBar>(child);
					if (sb) {
						return sb->callOnScroll(LOWORD(wParam), HIWORD(wParam));
					}
					auto tb = std::dynamic_pointer_cast<Trackbar>(child);
					if (tb) {
						return tb->callOnScroll(LOWORD(wParam), HIWORD(wParam));
					}
				}
			}
		}
		else {
			return m_onHScrollCb ? m_onHScrollCb(LOWORD(wParam), HIWORD(wParam)) : defaultOnScroll(false, LOWORD(wParam), HIWORD(wParam));
		}
		return 1;
	}


	int Window::defaultOnScroll(bool vertical, int mode, int pos)
	{
		SCROLLINFO si = {};
		si.cbSize = sizeof(SCROLLINFO);
		si.fMask = SIF_RANGE | SIF_POS | SIF_PAGE | SIF_TRACKPOS;
		::GetScrollInfo(m_hwnd, vertical ? SB_VERT : SB_HORZ, &si);
		int oldPos = si.nPos;
		switch (mode)
		{
		case SB_ENDSCROLL:
			break;
		case SB_LEFT:
			si.nPos = si.nMin;
			break;
		case SB_RIGHT:
			si.nPos = si.nMax;
			break;
		case SB_LINELEFT:
			si.nPos--;
			break;
		case SB_LINERIGHT:
			si.nPos++;
			break;
		case SB_PAGELEFT:
			si.nPos -= si.nPage;
			break;
		case SB_PAGERIGHT:
			si.nPos += si.nPage;
			break;
		case SB_THUMBPOSITION:
			si.nPos = si.nTrackPos;
			break;
		case SB_THUMBTRACK:
			si.nPos = si.nTrackPos;
			break;
		}
		if (oldPos != si.nPos)
		{
			si.fMask = SIF_POS;
			::SetScrollInfo(m_hwnd, vertical ? SB_VERT : SB_HORZ, &si, TRUE);
			if (vertical && m_onVScrollPosCb)
				m_onVScrollPosCb(getScrollPos(true));
			if (!vertical && m_onHScrollPosCb)
				m_onHScrollPosCb(getScrollPos(false));
		}
		return 0;
	}

	void Window::setupScroll(bool vertical, int pos, int min, int max, int page)
	{
		SCROLLINFO si = {};
		si.cbSize = sizeof(SCROLLINFO);
		si.fMask = SIF_RANGE | SIF_POS | SIF_PAGE;
		si.nMin = min;
		si.nMax = max + page - 1;
		si.nPos = pos;
		si.nPage = page;
		::SetScrollInfo(m_hwnd, vertical ? SB_VERT : SB_HORZ, &si, TRUE);
	}

	int Window::getScrollPos(bool vertical)
	{
		SCROLLINFO si = {};
		si.cbSize = sizeof(SCROLLINFO);
		si.fMask = SIF_POS;
		::GetScrollInfo(m_hwnd, vertical ? SB_VERT : SB_HORZ, &si);
		return si.nPos;
	}

	void Window::setScrollPos(bool vertical, int pos)
	{
		SCROLLINFO si = {};
		si.cbSize = sizeof(SCROLLINFO);
		si.fMask = SIF_POS;
		si.nPos = pos;
		::SetScrollInfo(m_hwnd, vertical ? SB_VERT : SB_HORZ, &si, TRUE);
	}

	void Window::setMenu(const Menu::Ptr& menu)
	{
		m_menu = menu;
		SetMenu(m_hwnd, menu->getHMENU());
	}

	void Window::setBigIcon(const Icon::Ptr& icon)
	{
		::SendMessage(m_hwnd, WM_SETICON, ICON_BIG, (LPARAM)icon->handle());
		m_bigIcon = icon;
	}

	void Window::setSmallIcon(const Icon::Ptr& icon)
	{
		::SendMessage(m_hwnd, WM_SETICON, ICON_SMALL, (LPARAM)icon->handle());
		m_smallIcon = icon;
	}

	Icon::Ptr Window::getSmallIcon() const
	{
		return m_smallIcon;
	}

	Icon::Ptr Window::getBigIcon() const
	{
		return m_bigIcon;
	}

	void Window::setCapture()
	{
		::SetCapture(m_hwnd);
	}

	void Window::releaseCapture()
	{
		::ReleaseCapture();
	}
}
