﻿#pragma once

#include <compare>
#include <memory>
#include <filesystem>
#include <vector>

#include "types.h"
#include "platform.h"

#include "easywin-rect.h"

namespace easywin
{
	class Bitmap
	{
	public:
		typedef std::shared_ptr<Bitmap> Ptr;
		typedef std::weak_ptr<Bitmap> WPtr;

		static Ptr CreateCompatibile(PaintDC& dc, int width, int height);
		static Ptr CreateCompatibile(PaintDC_Ptr& dc, int width, int height);

		static Ptr CreateFromBmpFile(const std::filesystem::path& filename);

		static Ptr CreateFromRGBAData(size_t w, size_t h, const std::vector<unsigned char>& pixels32bit);
		static Ptr CreateFromRGBAData(size_t w, size_t h, const std::vector<unsigned>& pixels32bit);
		static Ptr CreateEmpty(size_t w, size_t h);

		std::vector<unsigned char> getRGBA();
		Ptr getScalledCopy(float scale);
		Ptr getScalledCopy(size_t w, size_t h);

		Ptr crop(const Rect& rc);

		int width() { return m_bitmapInfo.bmWidth; }
		int height() { return m_bitmapInfo.bmHeight; }

		PaintDC_Ptr getPaintDC(PaintDC& baseDC);
		PaintDC_Ptr getPaintDC(PaintDC_Ptr& baseDC);
		PaintDC_Ptr getPaintDC();

		bool isHasTransparency() { return m_hMask != NULL; }
		void setTransparency(COLORREF bkColor);

		void draw(PaintDC& dc, int x, int y);
		void draw(PaintDC& dc, const Rect& dstRc);
		void alphaBlend(PaintDC& dc, int x, int y, int alpha);
		void alphaBlend(PaintDC& dc, const Rect& dstRc, int alpha);

		HBITMAP handle() const { return m_hbitmap; }
		HBITMAP handleMask() const { return m_hMask; }

		virtual ~Bitmap();
	private:
		Bitmap();
		HBITMAP m_hbitmap = NULL;
		HBITMAP m_hMask = NULL;
		BITMAP  m_bitmapInfo = {};
		PaintDC_WPtr m_bitmapDC;
	};
}