#pragma once

#include <compare>
#include <filesystem>
#include <string>

namespace easywin
{
	std::filesystem::path getAppDataFolderPath(const std::u8string& subDir);

	void openConsole();
	void closeConsole();
}