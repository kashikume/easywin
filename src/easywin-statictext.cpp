﻿#include <string>

#include "platform.h"
#include "types.h"

#include "easywin-statictext.h"

#include "easywin-window.h"

namespace easywin
{
	StaticText::Ptr StaticText::Create(const Window::Ptr& parent, const std::wstring& text, const RECT& rect, long style)
	{
		auto statictext = StaticText::Ptr(new StaticText);

		statictext->WindowBase::createWindow(
			L"STATIC",
			text,
			WS_CHILD | WS_VISIBLE | style,
			rect,
			parent);

		statictext->setupDefaultFont();

		return statictext;
	}

	void StaticText::setTextColor(COLORREF color)
	{
		m_textColor = color;
	}

	void StaticText::setBackground(COLORREF color)
	{
		CommandWindow::setBackground(color);
		m_backColor = color;
	}
}
