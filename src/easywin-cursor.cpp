#include "easywin-cursor.h"

namespace easywin
{
    Cursor::Ptr Cursor::CreateArrow()
    {
        return CreateSystemCursor(IDC_ARROW);
    }

    Cursor::Ptr Cursor::CreateAppStarting()
    {
        return CreateSystemCursor(IDC_APPSTARTING);
    }

    Cursor::Ptr Cursor::CreateCross()
    {
        return CreateSystemCursor(IDC_CROSS);
    }

    Cursor::Ptr Cursor::CreateHand()
    {
        return CreateSystemCursor(IDC_HAND);
    }

    Cursor::Ptr Cursor::CreateHelp()
    {
        return CreateSystemCursor(IDC_HELP);
    }

    Cursor::Ptr Cursor::CreateIBeam()
    {
        return CreateSystemCursor(IDC_IBEAM);
    }

    Cursor::Ptr Cursor::CreateNo()
    {
        return CreateSystemCursor(IDC_NO);
    }

    Cursor::Ptr Cursor::CreateSizeAll()
    {
        return CreateSystemCursor(IDC_SIZEALL);
    }

    Cursor::Ptr Cursor::CreateSizeNESW()
    {
        return CreateSystemCursor(IDC_SIZENESW);
    }

    Cursor::Ptr Cursor::CreateSizeNS()
    {
        return CreateSystemCursor(IDC_SIZENS);
    }

    Cursor::Ptr Cursor::CreateSizeNWSE()
    {
        return CreateSystemCursor(IDC_SIZENWSE);
    }

    Cursor::Ptr Cursor::CreateSizeWE()
    {
        return CreateSystemCursor(IDC_SIZEWE);
    }

    Cursor::Ptr Cursor::CreateUpArrow()
    {
        return CreateSystemCursor(IDC_UPARROW);
    }

    Cursor::Ptr Cursor::CreateWait()
    {
        return CreateSystemCursor(IDC_WAIT);
    }

    void Cursor::set()
    {
        ::SetCursor(m_hcursor);
    }

    Cursor::~Cursor()
    {
        if (m_requiresRemoval) {
            DestroyCursor(m_hcursor);
            m_hcursor = NULL;
        }
    }

    Cursor::Ptr Cursor::CreateSystemCursor(LPCWSTR cursorName)
    {
        Ptr out = std::make_shared<Cursor>();
        out->m_hcursor = LoadCursorW(NULL, cursorName);
        return out;
    }
}