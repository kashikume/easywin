#pragma once

#include "platform.h"

namespace easywin
{
	class Modifiers {
	public:
		Modifiers(WPARAM m) :_mod(m) {}

		bool isCtrlPressed() const { return _mod & MK_CONTROL; }
		bool isLeftButtonPressed() const { return _mod & MK_LBUTTON; }
		bool isMiddleButtonPressed() const { return _mod & MK_MBUTTON; }
		bool isRightButtonPressed() const { return _mod & MK_RBUTTON; }
		bool isShiftButtonPressed() const { return _mod & MK_SHIFT; }
		bool isX1ButtonPressed() const { return _mod & MK_XBUTTON1; }
		bool isX2ButtonPressed() const { return _mod & MK_XBUTTON2; }
	private:
		WPARAM _mod;
	};
}