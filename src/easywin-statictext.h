﻿#pragma once

#include <memory>
#include <string>

#include "types.h"
#include "platform.h"

#include "easywin-commandwindow.h"
#include "easywin-window.h"

namespace easywin
{
	class StaticText: public CommandWindow
	{
	public:
		typedef std::shared_ptr<StaticText> Ptr;
		typedef std::weak_ptr<StaticText> WPtr;

		static StaticText::Ptr Create(const Window::Ptr& parent, const std::wstring& u8text, const RECT& rect, long style = 0);

		void setTextColor(COLORREF color);
		virtual void setBackground(COLORREF color) override;

	protected:
		SPStaticText getReference() { return std::dynamic_pointer_cast<StaticText>(shared_from_this()); }
	private:
		StaticText() {}
	};
}
