#pragma once

#include <vector>

namespace easywin
{
	class BitmapTools
	{
	public:
		BitmapTools(size_t w, size_t h, const std::vector<unsigned char>& dataRGBA);

		std::vector<unsigned char> scaleTo(size_t w, size_t h);

	private:

		enum Subpixel
		{
			B = 0,
			G = 1,
			R = 2,
			A = 3
		};
		template<class T>
		struct BitmapData
		{
			T dataRGBA;
			size_t width = 0;
			size_t height = 0;

			void resize(size_t w, size_t h) { width = w, height = h, dataRGBA.resize(w * h * 4L); }

			unsigned char getSubpixel(size_t x, size_t y, Subpixel s) const { return dataRGBA[y * width * 4L + x * 4 + s]; }
			void setSubpixel(size_t x, size_t y, Subpixel s, unsigned char val) { dataRGBA[y * width * 4L + x * 4 + s] = val; }
		};

		BitmapData < const std::vector<unsigned char>&> _inData;
		BitmapData < std::vector<unsigned char>> _outData;
	};
}