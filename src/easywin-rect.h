#pragma once

#include <initializer_list>
#include "platform.h"

namespace easywin
{
	class Rect : public RECT
	{
	public:
		Rect(const RECT& r) {
			left = r.left;
			right = r.right;
			top = r.top;
			bottom = r.bottom;
		}
		Rect(const std::initializer_list<int>& l) {
			auto i = l.begin();
			if (i != l.end()) left = *(i++); else left = 0;
			if (i != l.end()) top = *(i++); else top = 0;
			if (i != l.end()) right = *(i++); else right = 0;
			if (i != l.end()) bottom = *(i++); else bottom = 0;
		}
		Rect() {}
		auto width() const { return right - left; }
		auto height() const { return bottom - top; }

		void moveBy(int dx, int dy) {
			left += dx;
			right += dx;
			top += dy;
			bottom += dy;
		}

		void relayoutToDpi(int newDpi, int oldDpi) {
			left = ::MulDiv(left, newDpi, oldDpi);
			right = ::MulDiv(right, newDpi, oldDpi);
			top = ::MulDiv(top, newDpi, oldDpi);
			bottom = ::MulDiv(bottom, newDpi, oldDpi);
		}
	};
}
