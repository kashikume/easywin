#pragma once

#include <memory>
#include <string>

#include "platform.h"
#include "easywin-consts.h"

namespace easywin
{
	struct FontSetup
	{
		int     height = -10;
		int     width = 0;
		int     escapement = 0;
		int     orientation = 0;
		int     weight = 0;
		bool    italic = false;
		bool    underline = false;
		bool    strikeOut = false;
		DWORD   charSet = DEFAULT_CHARSET;
		DWORD   outPrecision = OUT_DEFAULT_PRECIS;
		DWORD   clipPrecision = CLIP_DEFAULT_PRECIS;
		DWORD   quality = DEFAULT_QUALITY;
		DWORD   pitchAndFamily = DEFAULT_PITCH;
		std::u8string u8FontFaceName;
		int	    dpi = DefaultDpi;
	};

	class Font
	{
	public:
		typedef std::shared_ptr<Font> Ptr;

		static Ptr Create(const FontSetup& fs);
		~Font();
		const HFONT getHFont() const { return m_hfont; }
	private:
		Font(const FontSetup&);
		HFONT m_hfont;
		FontSetup m_setup;
	};
}