#pragma once

#include <initializer_list>
#include "platform.h"

namespace easywin
{
	class Point : public POINT
	{
	public:
		Point(const POINT& p)
		{
			x = p.x;
			y = p.y;
		}

		Point(int _x, int _y)
		{
			x = _x;
			y = _y;
		}

		Point(const std::initializer_list<int>& l) {
			auto i = l.begin();
			if (i != l.end()) x = *(i++); else x = 0;
			if (i != l.end()) y = *(i++); else y = 0;
		}

	};
}