#pragma once

#include <memory>
#include <functional>

#include "types.h"

#include "easywin-commandwindow.h"
#include "easywin-rect.h"
#include "easywin-window.h"

namespace easywin
{
	class Trackbar : public CommandWindow
	{
	public:
		typedef std::shared_ptr<Trackbar> Ptr;
		typedef std::weak_ptr<Trackbar> WPtr;

		static Trackbar::Ptr Create(Window::Ptr& parent, const Rect& rect, bool vertical);

		void setRange(int rmin, int rmax);
		void setPageSize(int page);
		void setPos(int pos);
		void setTicFreq(int freq);

		int getPos();

		int callOnScroll(short mode, short pos);

		void setOnCommand(const std::function<int()>& fun) { CommandWindow::setOnCommand(0, fun); }

	protected:
		Trackbar::Ptr getReference() { return std::dynamic_pointer_cast<Trackbar>(shared_from_this()); }
	private:
		Trackbar() {}

	};
}