#include <string>
#include <vector>
#include "platform.h"

#include "easywin-utf8.h"

namespace easywin
{
	std::wstring utf8ToWStr(const std::string& in)
	{
		std::vector<WCHAR> buffer(in.size() + 1, 0);
		auto numChar = MultiByteToWideChar(CP_UTF8, 0L, in.c_str(), (int)in.size(), buffer.data(), (int)buffer.size());
		return std::wstring(buffer.data());
	}

	std::wstring utf8ToWStr(const std::u8string& in)
	{
		std::vector<WCHAR> buffer(in.size() + 1, 0);
		auto numChar = MultiByteToWideChar(CP_UTF8, 0L, (char*)in.c_str(), (int)in.size(), buffer.data(), (int)buffer.size());
		return std::wstring(buffer.data());
	}

	std::u8string wStrToUtf8(const std::wstring& in)
	{
		std::vector<char8_t> buffer(in.size() * 4 + 1, 0);
		auto numChar = WideCharToMultiByte(CP_UTF8, 0L, in.c_str(), (int)in.size(), (char*)buffer.data(), (int)buffer.size(), NULL, NULL);
		return std::u8string(buffer.data());
	}
}