﻿#pragma once

#include <list>
#include <memory>
#include <string>

#include "platform.h"
#include "types.h"

#include "easywin-bitmap.h"
#include "easywin-rect.h"
#include "easywin-font.h"

namespace easywin
{
	class WindowBase : public std::enable_shared_from_this<WindowBase>
	{
	public:
		typedef std::shared_ptr<WindowBase> Ptr;
		typedef std::weak_ptr<WindowBase> WPtr;

		HWND getHWnd() const { return m_hwnd; }

		void setName(const std::u8string& name) { setText(name); };
		void setName(const std::wstring& name) { setText(name); }

		void setText(const std::u8string& text);
		void setText(const std::wstring& text);

		std::u8string getTextU8();
		std::wstring getText();

		virtual void setBitmap(const Bitmap::Ptr& bitmap);

		bool show() const { return ShowWindow(m_hwnd, SW_SHOW); }
		bool hide() const { return ShowWindow(m_hwnd, SW_HIDE); }
		bool isShown() const { return GetWindowLong(m_hwnd, GWL_STYLE) & WS_VISIBLE; }

		bool showMaximized() const { return ShowWindow(m_hwnd, SW_MAXIMIZE); }

		bool update() const { return UpdateWindow(m_hwnd); }

		void enable() { ::EnableWindow(m_hwnd, true); }
		void disable() { ::EnableWindow(m_hwnd, false); }
		void setEnabled(bool enabled) { ::EnableWindow(m_hwnd, enabled); }

		void setFocus();

		void invalidate() { InvalidateRect(m_hwnd, NULL, false); }

		virtual void updateLayout() {};

		void setSize(int w, int h);

		void moveWindow(const RECT& rc, bool repaint = true);
		void moveWindow(int x, int y, int w, int h, bool repaint = true);

		Rect getWindowRect();
		Rect getClientRect();

		int getDpiForWindow();

		void destroy();

		FontSetup getDefaultFontSetup();
		void setupDefaultFont();
		void setFont(const Font::Ptr& font);
		Font::Ptr getFont();

		int showMessageBox(const std::wstring& u8Text, const std::wstring& u8Caption, UINT type = MB_OK);

		void addChild(const WindowBase::Ptr& win);
		void removeChild(HWND hwnd);

		WindowBase::Ptr getParent() const { return m_parent.lock(); }
		WindowBase::Ptr getTopParent();

		WindowBase::Ptr getWindowBase() { return shared_from_this(); }

		std::wstring describe();

		void postMessage(UINT message, WPARAM wParam, LPARAM lParam);

		virtual ~WindowBase();
	protected:
		HWND m_hwnd = NULL;
		Font::Ptr m_font = nullptr;
		std::list< WindowBase::Ptr > m_children;
		WindowBase::WPtr m_parent;
		std::wstring m_windowClassName;

		bool createWindow(const std::wstring& className, const std::wstring& windowName, DWORD dwStyle, const Rect& rect, WindowBase::Ptr parent, DWORD dwExStyle = 0, LPVOID lpParam = NULL);

		void removeSelf();
	};
}