﻿#pragma once

#include <memory>
#include <string>

#include "types.h"
#include "platform.h"

#include "easywin-commandwindow.h"
#include "easywin-window.h"

namespace easywin
{
	class StaticImage: public CommandWindow
	{
	public:
		typedef std::shared_ptr<StaticImage> Ptr;
		typedef std::weak_ptr<StaticImage> WPtr;

		static StaticImage::Ptr Create(const Window::Ptr& parent, const std::wstring& text, const RECT& rect, long style = 0);
		static StaticImage::Ptr Create(const Window::Ptr& parent, const Bitmap::Ptr& bitmap, const RECT& rect, long style = 0);

		virtual void setBackground(COLORREF color) override;

		void setBitmap(const Bitmap::Ptr& bitmap) override;
	protected:
		Ptr getReference() { return std::dynamic_pointer_cast<StaticImage>(shared_from_this()); }
	private:
		StaticImage() {}
	};
}
