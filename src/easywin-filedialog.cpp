#include <string>

#include "platform.h"
#include "types.h"

#include "easywin-filedialog.h"

#include "easywin-utf8.h"
#include "easywin-window.h"

#pragma comment(linker, "\"/manifestdependency:type='Win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

namespace easywin
{

	class DialogEventHandler : public IFileDialogEvents,
		public IFileDialogControlEvents
	{
	public:
		// IUnknown methods
		IFACEMETHODIMP QueryInterface(REFIID riid, void** ppv)
		{
			static const QITAB qit[] = {
				QITABENT(DialogEventHandler, IFileDialogEvents),
				QITABENT(DialogEventHandler, IFileDialogControlEvents),
				{ 0 },
			};
			return QISearch(this, qit, riid, ppv);
		}

		IFACEMETHODIMP_(ULONG) AddRef()
		{
			return InterlockedIncrement(&_cRef);
		}

		IFACEMETHODIMP_(ULONG) Release()
		{
			long cRef = InterlockedDecrement(&_cRef);
			if (!cRef)
				delete this;
			return cRef;
		}

		// IFileDialogEvents methods
		IFACEMETHODIMP OnFileOk(IFileDialog*) { return S_OK; };
		IFACEMETHODIMP OnFolderChange(IFileDialog*) { return S_OK; };
		IFACEMETHODIMP OnFolderChanging(IFileDialog*, IShellItem*) { return S_OK; };
		IFACEMETHODIMP OnHelp(IFileDialog*) { return S_OK; };
		IFACEMETHODIMP OnSelectionChange(IFileDialog*) { return S_OK; };
		IFACEMETHODIMP OnShareViolation(IFileDialog*, IShellItem*, FDE_SHAREVIOLATION_RESPONSE*) { return S_OK; };
		IFACEMETHODIMP OnTypeChange(IFileDialog* pfd) { return S_OK; }
		IFACEMETHODIMP OnOverwrite(IFileDialog*, IShellItem*, FDE_OVERWRITE_RESPONSE*) { return S_OK; };

		// IFileDialogControlEvents methods
		IFACEMETHODIMP OnItemSelected(IFileDialogCustomize* pfdc, DWORD dwIDCtl, DWORD dwIDItem) { return S_OK; };
		IFACEMETHODIMP OnButtonClicked(IFileDialogCustomize*, DWORD) { return S_OK; };
		IFACEMETHODIMP OnCheckButtonToggled(IFileDialogCustomize*, DWORD, BOOL) { return S_OK; };
		IFACEMETHODIMP OnControlActivating(IFileDialogCustomize*, DWORD) { return S_OK; };

		DialogEventHandler() : _cRef(1) { };
	private:
		~DialogEventHandler() { };
		long _cRef;
	};

	HRESULT FileDialog::CreateInstance(REFIID riid, void** ppv)
	{
		*ppv = NULL;
		DialogEventHandler* pDialogEventHandler = new DialogEventHandler();
		HRESULT hr = pDialogEventHandler ? S_OK : E_OUTOFMEMORY;
		if (SUCCEEDED(hr)) {
			hr = pDialogEventHandler->QueryInterface(riid, ppv);
			pDialogEventHandler->Release();
		}
		return hr;
	}

	FileDialog::FileDialog()
	{
		fileTypesSelectorData.update({ {L"",L"*.*"} }, 0, L"");
	}

	bool FileDialog::showDialog(const Window::Ptr& parent, const IID& rclsid, DWORD flags)
	{
		// CoCreate the File Open Dialog object.
		IFileDialog* pfd = NULL;
		IFileDialogEvents* pfde = NULL;
		DWORD dwCookie = 0;
		DWORD dwFlags;

		HRESULT hr = CoInitializeEx(NULL, COINIT_MULTITHREADED);

		if (SUCCEEDED(hr))
			hr = CoCreateInstance(rclsid, NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&pfd));

		// Create an event handling object, and hook it up to the dialog.
		if (SUCCEEDED(hr))
			hr = CreateInstance(IID_PPV_ARGS(&pfde));

		// Hook up the event handler.
		if (SUCCEEDED(hr))
			hr = pfd->Advise(pfde, &dwCookie);

		if (SUCCEEDED(hr))
			hr = pfd->GetOptions(&dwFlags);

		if (SUCCEEDED(hr))
			hr = pfd->SetOptions(dwFlags | flags);

		if (SUCCEEDED(hr) && (flags & FOS_PICKFOLDERS) == 0)
			hr = pfd->SetFileTypes((UINT)fileTypesSelectorData.size(), fileTypesSelectorData.get());

		if (SUCCEEDED(hr) && (flags & FOS_PICKFOLDERS) == 0)
			hr = pfd->SetFileTypeIndex(fileTypesSelectorData.getDefaultSelection());

		if (SUCCEEDED(hr) && (flags & FOS_PICKFOLDERS) == 0)
			hr = pfd->SetDefaultExtension(fileTypesSelectorData.getDefaultExtension().c_str());

		if (SUCCEEDED(hr) && m_folder.has_value()) {
			IShellItem* folder;
			hr = SHCreateItemFromParsingName(m_folder->wstring().c_str(), NULL, IID_PPV_ARGS(&folder));
			if (SUCCEEDED(hr))
				hr = pfd->SetFolder(folder);
		}

		if (SUCCEEDED(hr) && m_fileName.has_value() && (flags & FOS_PICKFOLDERS) == 0) {
			hr = pfd->SetFileName(m_fileName->c_str());
		}

		// Show the dialog
		if (SUCCEEDED(hr))
			hr = pfd->Show(/*parent != nullptr ? parent->getHWnd() :*/ nullptr);

		if (SUCCEEDED(hr)) {
			IShellItem* psiResult;
			hr = pfd->GetResult(&psiResult);
			if (SUCCEEDED(hr)) {
				PWSTR pszFilePath = NULL;
				hr = psiResult->GetDisplayName(SIGDN_FILESYSPATH, &pszFilePath);
				m_result = std::filesystem::path(pszFilePath);
				psiResult->Release();
			}
		}

		// Unhook the event handler.
		if (pfd != nullptr)  pfd->Unadvise(dwCookie);
		if (pfde != nullptr) pfde->Release();
		if (pfd != nullptr)  pfd->Release();
		return SUCCEEDED(hr);
	}

	bool FileDialog::showFileSaveDialog(const Window::Ptr& parent)
	{
		return showDialog(parent, CLSID_FileSaveDialog, FOS_FORCEFILESYSTEM | FOS_FILEMUSTEXIST);
	}

	bool FileDialog::showFolderSelectDialog(const Window::Ptr& parent)
	{
		return showDialog(parent, CLSID_FileOpenDialog, FOS_FORCEFILESYSTEM | FOS_PICKFOLDERS);
	}

	void FileDialog::setFolder(const std::filesystem::path& path)
	{
		m_folder = path;
	}

	void FileDialog::setFileName(const std::wstring& filename)
	{
		m_fileName = filename;
	}

	bool FileDialog::showFileOpenDialog(const Window::Ptr& parent)
	{
		return showDialog(parent, CLSID_FileOpenDialog, FOS_FORCEFILESYSTEM | FOS_OVERWRITEPROMPT);
	}


	void FileDialog::SelectorData::update(const selector_type& value, int _defaultSelection, const std::wstring& _defaultExtension)
	{
		release();
		data.reserve(value.size());
		for (auto& d : value) {
			const auto& wf = d.first;
			wchar_t* left = new wchar_t[wf.size() + 1];
			wcscpy_s(left, wf.size() + 1, wf.c_str());

			const auto& ws = d.second;
			wchar_t* right = new wchar_t[ws.size() + 1];
			wcscpy_s(right, ws.size() + 1, ws.c_str());
			data.push_back({ left, right });
		}
		defaultSelection = _defaultSelection;
		defaultExtension = _defaultExtension;
	}


	FileDialog::SelectorData::SelectorData(const selector_type& value)
	{
		data.reserve(value.size());
		for (auto& d : value) {
			const auto& wf = d.first;
			wchar_t* left = new wchar_t[wf.size() + 1];
			wcscpy_s(left, wf.size() + 1, wf.c_str());

			const auto& ws = d.second;
			wchar_t* right = new wchar_t[ws.size() + 1];
			wcscpy_s(right, ws.size() + 1, ws.c_str());
			data.push_back({ left, right });
		}
	}

	void FileDialog::SelectorData::release()
	{
		for (auto& d : data) {
			delete[] d.pszName;
			delete[] d.pszSpec;
		}
		data.clear();
	}

	FileDialog::SelectorData::~SelectorData()
	{
		release();
	}

}