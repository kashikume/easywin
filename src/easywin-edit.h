#pragma once

#include <memory>
#include <functional>

#include "types.h"
#include "platform.h"

#include "easywin-commandwindow.h"
#include "easywin-window.h"

namespace easywin
{
	class Edit : public CommandWindow
	{
	public:
		typedef std::shared_ptr<Edit> Ptr;

		static Ptr Create(const Window_Ptr& parent, const RECT& rect, DWORD style);
		static Ptr CreateSingleLine(const Window::Ptr& parent, const RECT& rect) { return Create(parent, rect, WS_TABSTOP | WS_BORDER | WS_CHILD | WS_VISIBLE | ES_LEFT); };
		static Ptr CreateMultiline(const Window::Ptr& parent, const RECT& rect) { return Create(parent, rect, WS_TABSTOP | WS_BORDER | WS_CHILD | WS_VISIBLE | WS_VSCROLL | ES_LEFT | ES_MULTILINE | ES_AUTOVSCROLL); };
		static Ptr CreateMultilineReadOnly(const Window::Ptr& parent, const RECT& rect) { return Create(parent, rect, WS_TABSTOP | WS_BORDER | WS_CHILD | WS_VISIBLE | WS_VSCROLL | ES_LEFT | ES_MULTILINE | ES_AUTOVSCROLL | ES_READONLY); };
		static Ptr CreatePassword(const Window::Ptr& parent, const RECT& rect) { return Create(parent, rect, WS_TABSTOP | WS_BORDER | WS_CHILD | WS_VISIBLE | ES_LEFT | ES_PASSWORD); };

		virtual void setOnChange(const std::function<void()>& fun) override { CommandWindow::setOnCommand(EN_CHANGE, fun); }
		virtual void setOnSetFocus(const std::function<void()>& fun) override { CommandWindow::setOnCommand(EN_SETFOCUS, fun); }
		virtual void setOnKillFocus(const std::function<void()>& fun) override { CommandWindow::setOnCommand(EN_KILLFOCUS, fun); }

		void scrollLineUp();
		void scrollLineDown();
		void scrollPageUp();
		void scrollPageDown();
		void scrollToCarret();
		void scrollToLine(int line);
		void appendText(const std::wstring& text);

	protected:
		Ptr getReference() { return std::dynamic_pointer_cast<Edit>(shared_from_this()); }
	private:
		Edit() {}
	};
}
