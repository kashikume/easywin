#include "platform.h"
#include "types.h"

#include "easywin-edit.h"

#include "easywin-window.h"
#include "easywin-windowbase.h"

namespace easywin
{
	Edit_Ptr Edit::Create(const Window_Ptr& parent, const RECT& rect, DWORD style)
	{
		auto edit = Edit_Ptr(new Edit());

		edit->WindowBase::createWindow(
			L"EDIT",
			L"",
			style,
			rect,
			parent);

		edit->setupDefaultFont();

		return edit;
	}

	void Edit::scrollLineUp()
	{
		::SendMessage(m_hwnd, EM_SCROLL, SB_LINEUP, 0);
	}

	void Edit::scrollLineDown()
	{
		::SendMessage(m_hwnd, EM_SCROLL, SB_LINEDOWN, 0);
	}

	void Edit::scrollPageUp()
	{
		::SendMessage(m_hwnd, EM_SCROLL, SB_PAGEUP, 0);
	}

	void Edit::scrollPageDown()
	{
		::SendMessage(m_hwnd, EM_SCROLL, SB_PAGEDOWN, 0);
	}

	void Edit::scrollToCarret()
	{
		::SendMessage(m_hwnd, EM_SCROLLCARET, 0, 0);
	}

	void Edit::scrollToLine(int line)
	{
		::SendMessage(m_hwnd, EM_LINESCROLL, 0, line);
	}

	void Edit::appendText(const std::wstring& text)
	{
		int index = GetWindowTextLength(m_hwnd);
		SetFocus(m_hwnd); // set focus
		SendMessageW(m_hwnd, EM_SETSEL, (WPARAM)index, (LPARAM)index);
		SendMessageW(m_hwnd, EM_REPLACESEL, 0, (LPARAM)text.c_str());
	}
}
