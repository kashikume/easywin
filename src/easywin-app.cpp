#include <cassert>

#include "types.h"
#include "platform.h"

//#define _DEBUG_WINDOWS 1

#include "easywin-app.h"
#include "easywin-windowbase.h"
#include "easywin-window.h"


namespace easywin
{
	theApp::~theApp()
	{
		while (!m_windows.empty()) {
#if _DEBUG_WINDOWS
			std::cout << "Remaining window found: " << windows.begin()->second->describe() << std::endl;
#endif
			m_windows.begin()->second->destroy();
		}
	}

	theApp& theApp::getInstance()
	{
		static theApp instance;
		return instance;
	}

	WindowBase::Ptr theApp::getWindowBase(HWND hwnd) const
	{
		auto pos = m_windows.find(hwnd);

		if (pos == m_windows.end()) {
			return nullptr;
		}

		return pos->second;
	}

	Window_Ptr theApp::getWindow(HWND hwnd) const
	{
		return  std::dynamic_pointer_cast<Window>(getWindowBase(hwnd));
	}

	void theApp::addWindow(const WindowBase::Ptr& window)
	{
		assert(m_windows[window->getHWnd()] == nullptr);
		m_windows[window->getHWnd()] = window;
#if _DEBUG_WINDOWS
		std::cout << "Adding window: " << window->describe() << std::endl;
#endif
	}

	void theApp::removeWindow(HWND hwnd)
	{
#if _DEBUG_WINDOWS
		{
			auto f = windows.find(hwnd);
			if (f != windows.end())
			{
				std::cout << "Removing window: " << f->second->describe() << std::endl;
			}
		}
#endif
		// assert(windows.find(hwnd) != windows.end());
		m_windows.erase(hwnd);
	}

	void theApp::pushMessageLoop()
	{
		for (auto& w : m_windows)
			::PostMessage(w.second->getHWnd(), WM_NULL, 0, 0);
	}
}