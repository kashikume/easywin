﻿#pragma once

#include <memory>
#include <functional>
#include <map>
#include <optional>

#include <Windowsx.h>

#include "types.h"
#include "platform.h"

#include "easywin-cursor.h"
#include "easywin-icon.h"
#include "easywin-menu.h"
#include "easywin-modifiers.h"
#include "easywin-paintdc.h"
#include "easywin-windowbase.h"

namespace easywin
{
	enum class WindowShowStyle
	{
		normal,
		minimized,
		maximized
	};

	class Window : public WindowBase, MenuChangeListener
	{
	public:
		typedef std::shared_ptr<Window> Ptr;
		typedef std::weak_ptr<Window> WPtr;

		typedef std::function<bool(Window::Ptr&, bool doubleBuffer)> SetPixelFormatFunction;

		static Window::Ptr CreateTopWindow(bool verticalScroll = false, bool horisontalScroll = false);
		static Window::Ptr CreateClient(WindowBase::Ptr parent, const RECT& rect, bool verticalScroll = false, bool horisontalScroll = false);
		static Window::Ptr CreateFullScreenWindowGL(SetPixelFormatFunction setPxFormat = nullptr, bool doubleBuffer = true);
		static Window::Ptr CreateTopWindowGL(SetPixelFormatFunction setPxFormat = nullptr, bool doubleBuffer = true, bool verticalScroll = false, bool horisontalScroll = false);
		static Window::Ptr CreateClientGL(WindowBase::Ptr parent, const RECT& rect, SetPixelFormatFunction setPxFormat = nullptr, bool doubleBuffer = true, bool verticalScroll = false, bool horisontalScroll = false);
		static Window::Ptr CreateModalWindow(Window::Ptr parent, int w, int h, const std::function<void(Window::Ptr)>& createFun, bool verticalScroll = false, bool horisontalScroll = false);

		static void runMessageLoop();
		static void runMessageLoop(const std::function<void()>& idleFunction);
		static void enableIdle();
		static void disableIdle() { s_idleEnabled = false; }
		static bool isIdleEnabled() { return s_idleEnabled; }

		void coverFullScreen(const Window::Ptr& screen);

		void close();

		void setOnMessage(UINT message, const std::function < LRESULT(UINT, WPARAM, LPARAM) >& function) { m_messageFunctions[message] = function; }

		void setOnPaint(const std::function < LRESULT() >& function)
		{
			setOnMessage(WM_PAINT, [=](UINT, WPARAM, LPARAM)->LRESULT { return function(); });
		}

		void paintNow() { ::SendMessage(m_hwnd, WM_PAINT, 0, 0); }
		void setOnEreaseBg(const std::function < LRESULT(PaintDC&) >& function);

		void setOnDraw(const std::function < LRESULT(PaintDC&) >& function);
		void setOnBufferedDraw(const std::function < void(PaintDC::Ptr&) >& function);

		PaintDC::Ptr getDC();

		template <class T>
		void setOnClose(const std::function < T() >& function) { setOnMessage(WM_CLOSE, [=](UINT, WPARAM, LPARAM)->LRESULT { return function(); }); }
		void setOnClose(const std::function < void() >& function);

		template <class T>
		void setOnDestroy(const std::function < T() >& function) { setOnMessage(WM_DESTROY, [=](UINT, WPARAM, LPARAM)->LRESULT { return function(); }); }
		void setOnDestroy(const std::function <void()>& function);

		void setOnMove(const std::function<void(int x, int y)>& function);
		void setOnSize(const std::function<void(int width, int height, WindowShowStyle mode) >& function);

		void setOnSizing(const std::function< LRESULT(WPARAM edge, LPRECT rect) >& function)
		{
			setOnMessage(WM_SIZING, [=](UINT, WPARAM wParam, LPARAM lParam)->LRESULT { return function(wParam, (LPRECT)lParam); });
		}

		void setOnMouseLeftDown(const std::function < LRESULT(int x, int y, const Modifiers& modifiers) >& function) { setOnMouse(WM_LBUTTONDOWN, function); }
		void setOnMouseLeftUp(const std::function < LRESULT(int x, int y, const Modifiers& modifiers) >& function) { setOnMouse(WM_LBUTTONUP, function); }
		void setOnMouseRightDown(const std::function < LRESULT(int x, int y, const Modifiers& modifiers) >& function) { setOnMouse(WM_RBUTTONDOWN, function); }
		void setOnMouseRightUp(const std::function < LRESULT(int x, int y, const Modifiers& modifiers) >& function) { setOnMouse(WM_RBUTTONUP, function); }
		void setOnMouseMiddleDown(const std::function < LRESULT(int x, int y, const Modifiers& modifiers) >& function) { setOnMouse(WM_MBUTTONDOWN, function); }
		void setOnMouseMiddleUp(const std::function < LRESULT(int x, int y, const Modifiers& modifiers) >& function) { setOnMouse(WM_MBUTTONUP, function); }

		void setOnMouseWheel(const std::function <LRESULT(short move, const Modifiers& modifiers, int x, int y) >& function)
		{
			setOnMessage(WM_MOUSEWHEEL, [=](UINT, WPARAM wParam, LPARAM lParam)->LRESULT { return function((short)HIWORD(wParam), Modifiers(wParam), LOWORD(lParam), HIWORD(lParam)); });
		}

		void setOnMouseMove(const std::function < LRESULT(int x, int y, const Modifiers& modifiers) >& function) { setOnMouse(WM_MOUSEMOVE, function); }
		void setOnChar(const std::function<LRESULT(WCHAR)>& function) { setOnMessage(WM_CHAR, [=](UINT, WPARAM wParam, LPARAM)->LRESULT { return function((WCHAR)wParam); }); }
		void setOnKeyDown(const std::function<LRESULT(int)>& function) { setOnMessage(WM_KEYDOWN, [=](UINT, WPARAM wParam, LPARAM)->LRESULT { return function((int)wParam); }); }
		void setOnKeyUp(const std::function<LRESULT(WPARAM)>& function) { setOnMessage(WM_KEYUP, [=](UINT, WPARAM wParam, LPARAM)->LRESULT { return function(wParam); }); }
		void setOnSysKeyDown(const std::function<LRESULT(int)>& function);
		void setOnSysKeyUp(const std::function<LRESULT(WPARAM)>& function);

		//timer
		int startTimer(int interval, const std::function < void() >& function);
		void stopTimer(int id);
		int callDelayed(int delay, const std::function < void() >& function);

		LRESULT onCommand(WPARAM wParam, LPARAM lParam);
		LRESULT onCtrlColor(UINT message, WPARAM wParam, LPARAM lParam);
		LRESULT onNotify(WPARAM wParam, LPARAM lParam);
		LRESULT onHScroll(WPARAM wParam, LPARAM lParam);
		LRESULT onVScroll(WPARAM wParam, LPARAM lParam);
		LRESULT onSetCursor();

		void setonDpiChanged(const std::function<LRESULT(int dpi, const Rect& rc)>& function);
		LRESULT onDpiChangedProc(int dpi, const Rect& rc);

		void setOnVScroll(std::function<int(int mode, int pos)>fun) { m_onVScrollCb = fun; }
		void setOnHScroll(std::function<int(int mode, int pos)>fun) { m_onHScrollCb = fun; }
		void setOnVScrollPos(std::function<int(int pos)>fun) { m_onVScrollPosCb = fun; }
		void setOnHScrollPos(std::function<int(int pos)>fun) { m_onHScrollPosCb = fun; }

		int defaultOnScroll(bool vertical, int mode, int pos);
		void setupScroll(bool vertical, int pos, int min, int max, int page);
		int getScrollPos(bool vertical);
		void setScrollPos(bool vertical, int pos);

		void setMenu(const Menu::Ptr& menu);
		void setBigIcon(const Icon::Ptr& icon);
		void setSmallIcon(const Icon::Ptr& icon);
		void setIcon(const Icon::Ptr& icon) { setBigIcon(icon); setSmallIcon(icon); }
		void setCursor(const Cursor::Ptr& cursor) { m_cursor = cursor; }
		Icon::Ptr getSmallIcon() const;
		Icon::Ptr getBigIcon() const;

		void setCapture();
		void releaseCapture();

		enum class Feature
		{
			resizeFrame,
			minimizeBox,
			maximizeBox,
			sysMenu,
			caption
		};

		void setFeature(const Feature& feature, bool enable);

		inline void setMinHeight(unsigned height) { m_minHeight = height; }
		inline void resetMinHeight() { m_minHeight.reset(); }
		inline void setMinWidth(unsigned width) { m_minWidth = width; }
		inline void resetMinWidth() { m_minWidth.reset(); }

	protected:
		void registerClass();
		void registerClassGL();

		void registerDefaultMessages();
		void setupPixelFormatGL(bool doubleBuffer);

		void callTimerFunction(int id);
		void setOnMouse(UINT message, const std::function< LRESULT(int x, int y, const Modifiers& modifiers) >& function) { setOnMessage(message, [=](UINT, WPARAM wParam, LPARAM lParam)->LRESULT { return function(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam), Modifiers(wParam)); }); }

		LRESULT wndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);

		LRESULT onGetMinMaxInfo(UINT, WPARAM, LPARAM lParam);

	protected:
		Window::Ptr getReference() { return std::dynamic_pointer_cast<Window>(shared_from_this()); }

	private:
		Window();
		Window(const Window&) = delete;
		int m_lastTimerId = 0;
		static int s_nextWindowClassId;

		static bool s_idleEnabled;

		std::map < UINT, std::function<LRESULT(UINT, WPARAM, LPARAM)> > m_messageFunctions;
		std::map < int, std::function<void()> > m_timerFunctions;

		Menu::Ptr m_menu;
		Icon::Ptr m_smallIcon, m_bigIcon;
		Cursor::Ptr m_cursor;

		std::optional<unsigned> m_minWidth;
		std::optional<unsigned> m_minHeight;

		std::function<int(int pos, int mode)> m_onHScrollCb = nullptr;
		std::function<int(int pos, int mode)> m_onVScrollCb = nullptr;
		std::function<int(int pos)> m_onHScrollPosCb = nullptr;
		std::function<int(int pos)> m_onVScrollPosCb = nullptr;
		std::function<void()> m_beforeDestroy = nullptr;

		SPBitmap m_drawingBuffer;

		// Inherited via MenuChangeListener
		virtual void onMenuUpdate(SPMenu menu) override;
	};
}