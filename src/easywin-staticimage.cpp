﻿#include <string>

#include "platform.h"
#include "types.h"

#include "easywin-staticimage.h"

#include "easywin-window.h"

namespace easywin
{
	StaticImage::Ptr StaticImage::Create(const Window::Ptr& parent, const std::wstring& text, const RECT& rect, long style)
	{
		auto staticimage = Ptr(new StaticImage);

		staticimage->WindowBase::createWindow(
			L"STATIC",
			text,
			WS_CHILD | WS_VISIBLE | SS_BITMAP | style,
			rect,
			parent);

		staticimage->setupDefaultFont();

		return staticimage;
	}

	StaticImage::Ptr StaticImage::Create(const Window::Ptr& parent, const Bitmap::Ptr& bitmap, const RECT& rect, long style)
	{
		auto ctrl = Create(parent, L"", rect, style);
		ctrl->setBitmap(bitmap);
		return ctrl;
	}

	void StaticImage::setBackground(COLORREF color)
	{
		CommandWindow::setBackground(color);
		m_backColor = color;
	}

	void StaticImage::setBitmap(const Bitmap::Ptr& bitmap)
	{
		SendMessage(
			(HWND)m_hwnd,
			(UINT)STM_SETIMAGE,
			(WPARAM)IMAGE_BITMAP,
			(LPARAM)bitmap->handle());
	}
}
