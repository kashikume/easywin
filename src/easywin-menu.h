#pragma once

#include <memory>
#include <string>
#include <functional>

#include "platform.h"
#include "types.h"

#include "easywin-utf8.h"

namespace easywin
{
	class MenuChangeListener
	{
	public:
		typedef std::shared_ptr<MenuChangeListener> Ptr;
		typedef std::weak_ptr<MenuChangeListener> WPtr;
		virtual void onMenuUpdate(Menu_Ptr menu) = 0;
	};

	class Menu : public MenuChangeListener, std::enable_shared_from_this<Menu>
	{
	public:
		typedef std::shared_ptr<Menu> Ptr;
		typedef std::weak_ptr<Menu> WPtr;

		static Menu::Ptr Create();
		~Menu();

		Ptr addSubMenu(const std::wstring& name);
		Ptr addSubMenu(const std::u8string& u8name) { return addSubMenu(utf8ToWStr(u8name)); }
		int addMenuItem(const std::wstring& name, std::function<void()> command);
		int addMenuItem(const std::u8string& u8name, std::function<void()> command) { return addMenuItem(utf8ToWStr(u8name), command); }
		int addMenuItem(const std::wstring& name, const std::wstring& shortcut, std::function<void()> command);
		int addMenuItem(const std::u8string& u8name, const std::u8string& u8shortcut, std::function<void()> command) { return addMenuItem(utf8ToWStr(u8name), utf8ToWStr(u8shortcut), command); }
		void addSeparator();

		void registerListener(MenuChangeListener::Ptr listener);
		void deregisterListener(MenuChangeListener::Ptr listener);

		HMENU getHMENU() { return m_hmenu; }

		// Inherited via MenuChangeListener
		virtual void onMenuUpdate(Menu::Ptr menu) override;

		void onCommand(int id);

		void setItemEnabled(int id, bool enabled);
		void setItemChecked(int id, bool checked);
		void setText(int id, const std::wstring& name, const std::wstring& shortcut);
		void setText(int id, const std::u8string& name, const std::u8string& shortcut) { setText(id, utf8ToWStr(name), utf8ToWStr(shortcut)); }
		void setName(int id, const std::wstring& name);
		void setName(int id, const std::u8string& name) { setName(id, utf8ToWStr(name)); }
		void setShortcut(int id, const std::wstring& shortcut);
		void setShortcut(int id, const std::u8string& shortcut) { setShortcut(id, utf8ToWStr(shortcut)); }

		std::wstring getText(int id);
		std::wstring getName(int id);
		std::wstring getShortcut(int id);
	private:
		HMENU m_hmenu = NULL;
		std::vector<MenuChangeListener::WPtr> m_listeners;
		std::unordered_map<int, std::function<void()>> m_commands;
		std::vector<Menu::Ptr> m_submenus;

		int getNextId();

		void notifyListeners();

		Menu() {}
	};
}