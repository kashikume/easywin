#pragma once
#include <memory>
#include <unordered_map>

#include "types.h"
#include "platform.h"

#include "easywin-windowbase.h"
#include "easywin-window.h"

namespace easywin
{
	class theApp
	{
	public:
		~theApp();
		static theApp& getInstance();
		Window::Ptr getWindow(HWND hwnd) const;
		WindowBase::Ptr getWindowBase(HWND hwnd) const;
		void addWindow(const WindowBase::Ptr& window);
		void removeWindow(HWND hwnd);
		void pushMessageLoop();
	protected:
	private:
		std::unordered_map< HWND, WindowBase_Ptr > m_windows;
	};
}
