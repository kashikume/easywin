#pragma once

#include <string>

namespace easywin
{
	class Bitmap;
	class PaintDC;
	class Rect;

	std::wstring utf8ToWStr(const std::string& in);
	std::wstring utf8ToWStr(const std::u8string& in);
	std::u8string wStrToUtf8(const std::wstring& in);
}