#include <memory>
#include <string>

#include "platform.h"
#include "types.h"

#include "easywin-button.h"

#include "easywin-rect.h"
#include "easywin-windowbase.h"
#include "easywin-window.h"

namespace easywin
{
	Button::Ptr Button::Create(const Window::Ptr& parent, const std::wstring& title, const Rect& rect, DWORD style)
	{
		auto button = Button::Ptr(new Button);

		button->WindowBase::createWindow(
			L"BUTTON",
			title,
			style,
			rect,
			parent);

		button->setupDefaultFont();

		return button;
	}

	Button::Ptr Button::CreateButton(const Window::Ptr& parent, const std::wstring& title, const SPBitmap& bitmap, const Rect& rect)
	{
		auto button = Button::Ptr(new Button);

		button->WindowBase::createWindow(
			L"BUTTON",
			title,
			BS_NOTIFY | BS_BITMAP | WS_VISIBLE | WS_CHILD | WS_TABSTOP | BS_PUSHBUTTON,
			rect,
			parent);

		button->setBitmap(bitmap);

		return button;
	}

	bool Button::isChecked()
	{
		bool isPush = (::GetWindowLong(m_hwnd, GWL_STYLE) & BS_TYPEMASK) == BS_PUSHBUTTON;
		if (isPush)
			return ::SendMessageW(m_hwnd, BM_GETSTATE, 0, 0) == BST_PUSHED;
		else
			return ::SendMessageW(m_hwnd, BM_GETCHECK, 0, 0) == BST_CHECKED;
	}

	void Button::setCheck(bool checked)
	{
		bool isPush = (::GetWindowLong(m_hwnd, GWL_STYLE) & BS_TYPEMASK) == BS_PUSHBUTTON;
		if (isPush)
			::SendMessageW(m_hwnd, BM_SETSTATE, checked ? BST_PUSHED : BST_UNCHECKED, 0);
		else
			::SendMessageW(m_hwnd, BM_SETCHECK, checked ? BST_CHECKED : BST_UNCHECKED, 0);
	}

	void Button::setBitmap(const Bitmap::Ptr& bitmap)
	{
		if (bitmap) {
			SendMessage(
				(HWND)m_hwnd,
				(UINT)BM_SETIMAGE,
				(WPARAM)IMAGE_BITMAP,
				(LPARAM)bitmap->handle());
		}
		else {
			SendMessage(
				(HWND)m_hwnd,
				(UINT)BM_SETIMAGE,
				(WPARAM)IMAGE_BITMAP,
				(LPARAM)NULL);
		}
	}
}
