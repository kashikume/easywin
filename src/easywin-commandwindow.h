﻿#pragma once

#include <functional>
#include <optional>
#include <map>

#include "platform.h"

#include "easywin-windowbase.h"

namespace easywin
{
	class CommandWindow : public WindowBase {
	public:

		virtual ~CommandWindow();

		virtual int onNotify(unsigned int code);
		int callOnCommand(unsigned notification);

		void setOnNotifyCb(unsigned int code, const std::function<void()>& fun);

		virtual void setOnChange(const std::function<void()>& fun);
		virtual void setOnOutOfMemory(const std::function<void()>& fun);
		virtual void setOnClick(const std::function<void()>& fun);
		virtual void setOnBbClick(const std::function<void()>& fun);
		virtual void setOnReturn(const std::function<void()>& fun);
		virtual void setOnRClick(const std::function<void()>& fun);
		virtual void setOnRDbClick(const std::function<void()>& fun);
		virtual void setOnSetFocus(const std::function<void()>& fun);
		virtual void setOnKillFocus(const std::function<void()>& fun);
		virtual void setOnCustomDraw(const std::function<void()>& fun);
		virtual void setOnHover(const std::function<void()>& fun);
		virtual void setOnNcHitTest(const std::function<void()>& fun);
		virtual void setOnKeyDown(const std::function<void()>& fun);
		virtual void setOnReleasedCapture(const std::function<void()>& fun);
		virtual void setOnSetCursor(const std::function<void()>& fun);
		virtual void setOnChar(const std::function<void()>& fun);
		virtual void setOnTooltipsCreated(const std::function<void()>& fun);
		virtual void setOnLDown(const std::function<void()>& fun);
		virtual void setOnRDown(const std::function<void()>& fun);
		virtual void setOnThemeChanged(const std::function<void()>& fun);

		virtual void setBackground(COLORREF color);
		void resetColors();
		HBRUSH getBackgroundBrush() const { return m_backgroundBrush; }

		std::optional<COLORREF> getBackgroundColor() const { return m_backColor; }
		std::optional<COLORREF> getTextColor() const { return m_textColor; }

		void addTooltip(const std::wstring& text, bool baloon = false);
		void addTooltip(const std::u8string& text, bool baloon = false);
		void removeTooltip();

	protected:
		void setOnCommand(unsigned notification, std::function<void()> fun);

		HBRUSH m_backgroundBrush = NULL;
		std::optional<COLORREF> m_backColor;
		std::optional<COLORREF> m_textColor;

	private:
		HWND m_tooltipWindowHandle = NULL;
		std::map<unsigned int, std::function<void()>> m_onCommandCb;
		std::map<unsigned int, std::function<void()> > m_onNotifyCb;
	};
}