﻿#pragma once

#include <memory>
#include <functional>

#include "types.h"
#include "platform.h"

#include "easywin-commandwindow.h"
#include "easywin-window.h"

namespace easywin
{
	class ScrollBar : public CommandWindow
	{
	public:
		typedef std::shared_ptr<ScrollBar> Ptr;
		typedef std::weak_ptr<ScrollBar> WPtr;

		static ScrollBar::Ptr Create(Window::Ptr& parent, const RECT& rect, bool vertical);

		void setOnScroll(std::function<int(int pos, int mode)>fun) { _onScrollCb = fun; }
		int defaultOnScroll(int mode, int pos);
		int callOnScroll(short mode, short pos) { return _onScrollCb ? _onScrollCb(mode, pos) : defaultOnScroll(mode, pos); }

		void setupScroll(int pos, int min, int max, int page);
		void setRange(int min, int max);
		void setPos(int pos);
		int getPos();
		int getMax();
		int getMin();
		std::pair<int, int> getRange();

		virtual void setOnChange(const std::function<void()>& fun) override { CommandWindow::setOnCommand(0, fun); }

	protected:
		SPScrollBar getReference() { return std::dynamic_pointer_cast<ScrollBar>(shared_from_this()); }

	private:
		bool _vertical;
		std::function<int(int mode, int pos)> _onScrollCb = nullptr;

		ScrollBar(bool vertical) : _vertical(vertical) {}
	};
}
