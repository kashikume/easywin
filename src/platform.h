#pragma once

#include <Windows.h>
#include <winhttp.h>
#include <CommCtrl.h>
#include <windowsx.h>
#include <shtypes.h>
#include <Shobjidl.h>
#include <shlwapi.h>

#ifdef max
#undef max
#endif

#ifdef min
#undef min
#endif
