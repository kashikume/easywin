﻿#include "types.h"
#include "platform.h"

#include "easywin-scrollbar.h"

#include "easywin-window.h"

namespace easywin
{
	ScrollBar::Ptr ScrollBar::Create(Window::Ptr& parent, const RECT& rect, bool vertical)
	{
		auto scrollbar = SPScrollBar(new ScrollBar(vertical));
		HINSTANCE hInstance = GetModuleHandle(NULL);

		scrollbar->WindowBase::createWindow(
			L"SCROLLBAR",
			L"",
			WS_CHILD | WS_VISIBLE | (vertical ? SBS_VERT : SBS_HORZ),
			rect,
			parent);

		return scrollbar;
	}

	int ScrollBar::defaultOnScroll(int mode, int pos)
	{
		SCROLLINFO si = {};
		si.cbSize = sizeof(SCROLLINFO);
		si.fMask = SIF_RANGE | SIF_POS | SIF_PAGE | SIF_TRACKPOS;
		::GetScrollInfo(m_hwnd, SB_CTL, &si);
		int oldPos = si.nPos;
		switch (mode)
		{
		case SB_ENDSCROLL:
			break;
		case SB_LEFT:
			si.nPos = si.nMin;
			break;
		case SB_RIGHT:
			si.nPos = si.nMax;
			break;
		case SB_LINELEFT:
			si.nPos--;
			break;
		case SB_LINERIGHT:
			si.nPos++;
			break;
		case SB_PAGELEFT:
			si.nPos -= si.nPage;
			break;
		case SB_PAGERIGHT:
			si.nPos += si.nPage;
			break;
		case SB_THUMBPOSITION:
			si.nPos = si.nTrackPos;
			break;
		case SB_THUMBTRACK:
			si.nPos = si.nTrackPos;
			break;
		}
		if (oldPos != si.nPos) {
			si.fMask = SIF_POS;
			::SetScrollInfo(m_hwnd, SB_CTL, &si, TRUE);
			callOnCommand(0);
		}
		return 0;
	}

	void ScrollBar::setupScroll(int pos, int min, int max, int page)
	{
		SCROLLINFO si = {};
		si.cbSize = sizeof(SCROLLINFO);
		si.fMask = SIF_RANGE | SIF_POS | SIF_PAGE;
		si.nMin = min;
		si.nMax = max + page - 1;
		si.nPos = pos;
		si.nPage = page;
		::SetScrollInfo(m_hwnd, SB_CTL, &si, TRUE);
	}

	void ScrollBar::setRange(int min, int max)
	{
		SCROLLINFO si = {};
		si.cbSize = sizeof(SCROLLINFO);
		si.fMask = SIF_RANGE;
		si.nMin = min;
		si.nMax = max;
		::SetScrollInfo(m_hwnd, SB_CTL, &si, TRUE);
	}

	void ScrollBar::setPos(int pos)
	{
		SCROLLINFO si = {};
		si.cbSize = sizeof(SCROLLINFO);
		si.fMask = SIF_POS;
		si.nPos = pos;
		::SetScrollInfo(m_hwnd, SB_CTL, &si, TRUE);
	}

	int ScrollBar::getPos()
	{
		SCROLLINFO si = {};
		si.cbSize = sizeof(SCROLLINFO);
		si.fMask = SIF_POS;
		::GetScrollInfo(m_hwnd, SB_CTL, &si);
		return si.nPos;;
	}

	int ScrollBar::getMax()
	{
		SCROLLINFO si = {};
		si.cbSize = sizeof(SCROLLINFO);
		si.fMask = SIF_RANGE;
		::GetScrollInfo(m_hwnd, SB_CTL, &si);
		return si.nMax;
	}

	int ScrollBar::getMin()
	{
		SCROLLINFO si = {};
		si.cbSize = sizeof(SCROLLINFO);
		si.fMask = SIF_RANGE;
		::GetScrollInfo(m_hwnd, SB_CTL, &si);
		return si.nMin;
	}

	std::pair<int, int> ScrollBar::getRange()
	{
		SCROLLINFO si = {};
		si.cbSize = sizeof(SCROLLINFO);
		si.fMask = SIF_RANGE;
		::GetScrollInfo(m_hwnd, SB_CTL, &si);
		return std::make_pair(si.nMin, si.nMax);
	}
}