#include <Shlobj.h>
#include <stdio.h>
#include <fcntl.h>
#include <io.h>

#include <compare>
#include <string>
#include <filesystem>
#include <iostream>

#include "platform.h"

#include "easywin-tools.h"

#include "easywin-utf8.h"

namespace easywin
{
	std::filesystem::path getAppDataFolderPath(const std::u8string& subDir)
	{
		WCHAR buffer[MAX_PATH];
		auto subDirW = utf8ToWStr(subDir);
		SHGetFolderPathAndSubDirW(NULL, CSIDL_FLAG_CREATE | CSIDL_LOCAL_APPDATA, NULL, SHGFP_TYPE_CURRENT, (WCHAR*)subDirW.c_str(), buffer);
		return wStrToUtf8(buffer);
	}


	void openConsole()
	{
		//Create a console for this application
		AllocConsole();

		bool result = true;
		FILE* fp;

		// Redirect STDIN if the console has an input handle
		if (GetStdHandle(STD_INPUT_HANDLE) != INVALID_HANDLE_VALUE)
			if (freopen_s(&fp, "CONIN$", "r", stdin) != 0)
				result = false;
			else
				setvbuf(stdin, NULL, _IONBF, 0);

		// Redirect STDOUT if the console has an output handle
		if (GetStdHandle(STD_OUTPUT_HANDLE) != INVALID_HANDLE_VALUE)
			if (freopen_s(&fp, "CONOUT$", "w", stdout) != 0)
				result = false;
			else
				setvbuf(stdout, NULL, _IONBF, 0);

		// Redirect STDERR if the console has an error handle
		if (GetStdHandle(STD_ERROR_HANDLE) != INVALID_HANDLE_VALUE)
			if (freopen_s(&fp, "CONOUT$", "w", stderr) != 0)
				result = false;
			else
				setvbuf(stderr, NULL, _IONBF, 0);

		// Make C++ standard streams point to console as well.
		std::ios::sync_with_stdio(true);

		// Clear the error state for each of the C++ standard streams.
		std::wcout.clear();
		std::cout.clear();
		std::wcerr.clear();
		std::cerr.clear();
		std::wcin.clear();
		std::cin.clear();
	}

	void closeConsole()
	{
		::FreeConsole();
	}
}