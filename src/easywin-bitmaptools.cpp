#include <utility>
#include <cmath>

#include "easywin-bitmaptools.h"

namespace easywin
{
	BitmapTools::BitmapTools(size_t w, size_t h, const std::vector<unsigned char>& dataRGBA)
		: _inData{ dataRGBA, w, h }
	{
	}

	std::vector<unsigned char> BitmapTools::scaleTo(size_t w, size_t h)
	{
		_outData.resize(w, h);

		float scaleX = (float)_inData.width / (float)w;
		float scaleY = (float)_inData.height / (float)h;

		for (size_t y = 0; y < h; y++) {
			float ymin = scaleY * y;
			float ymax = scaleY * (y + 1);

			std::vector<std::pair< int, float >> ys;
			if (ymin > std::floor(ymin))
				ys.push_back({ (int)ymin, 1.0f - (ymin - std::floor(ymin)) });
			for (float fy = std::ceil(ymin); fy < std::floor(ymax); fy += 1.0f)
				ys.push_back({ (int)fy, 1.0f });
			if (ymax > std::floor(ymax))
				ys.push_back({ (int)ymax, (ymax - std::floor(ymax)) });

			for (size_t x = 0; x < w; x++) {
				float xmin = scaleX * x;
				float xmax = scaleX * (x + 1);

				std::vector<std::pair< int, float >> xs;
				if (xmin > std::floor(xmin))
					xs.push_back({ (int)xmin, 1.0f - (xmin - std::floor(xmin)) });
				for (float fx = std::ceil(xmin); fx < std::floor(xmax); fx += 1.0f)
					xs.push_back({ (int)fx, 1.0f });
				if (xmax > std::floor(xmax))
					xs.push_back({ (int)xmax, (xmax - std::floor(xmax)) });

				for (int s = 0; s < 4; ++s) {
					std::pair< float, float > pixel;
					for (const auto& ay : ys) {
						if (ay.first < _inData.height && ay.first >= 0) {
							for (const auto& ax : xs) {
								if (ax.first < _inData.width && ax.first >= 0) {
									auto val = _inData.getSubpixel(ax.first, ay.first, (Subpixel)s);
									float weight = ax.second * ay.second;
									pixel.first += (float)val * weight;
									pixel.second += weight;
								}
							}
						}
					}
					_outData.setSubpixel(x, y, (Subpixel)s, (unsigned char)(pixel.first / pixel.second));
				}

			}
		}

		auto out(std::move(_outData.dataRGBA));
		_outData = {};
		return out;
	}
}