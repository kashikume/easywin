#include <initializer_list>
#include <memory>
#include <algorithm>
#include <string>
#include <vector>
#include "platform.h"

#include "easywin-paintdc.h"

#include "easywin-icon.h"
#include "easywin-bitmap.h"
#include "easywin-utf8.h"
#include "easywin-font.h"
#include "easywin-window.h"

namespace easywin
{

	PaintDC::PaintDC(HWND hwnd)
		: m_hwnd(hwnd)
		, m_callEndPaint(true)
		, m_requiresDeletation(false)
		, m_ps{}
	{
		m_hdc = BeginPaint(hwnd, &m_ps);
		selectStockBrush();
		selectStockPen();
	}

	PaintDC::PaintDC(HWND hwnd, HDC hdc)
		: m_hwnd(hwnd)
		, m_hdc(hdc)
		, m_callEndPaint(false)
		, m_requiresDeletation(false)
		, m_ps{}
	{
	}

	PaintDC::PaintDC(HDC hdc)
		: m_hwnd(NULL)
		, m_hdc(hdc)
		, m_callEndPaint(false)
		, m_requiresDeletation(false)
	{
	}

	PaintDC::~PaintDC()
	{
		std::for_each(m_oldObj.rbegin(), m_oldObj.rend(), [=](auto k) {
			SelectObject(m_hdc, k);
			});
		if (m_callEndPaint)
			EndPaint(m_hwnd, &m_ps);
		if (m_requiresDeletation)
			DeleteDC(m_hdc);
	}

	void PaintDC::drawCircle(int x, int y, int r)
	{
		Ellipse(m_hdc, x - r, y - r, x + r, y + r);
	}

	void PaintDC::drawRectangle(int x1, int y1, int x2, int y2)
	{
		Rectangle(m_hdc, x1, y1, x2, y2);
	}

	void PaintDC::drawRectangle(const Rect& rect)
	{
		Rectangle(m_hdc, rect.left, rect.top, rect.right, rect.bottom);
	}

	void  PaintDC::drawRoundedRectangle(const Rect& rect, int rw, int rh)
	{
		RoundRect(m_hdc, rect.left, rect.top, rect.right, rect.bottom, rw, rh);
	}

	void PaintDC::drawLine(int x1, int y1, int x2, int y2)
	{
		MoveToEx(m_hdc, x1, y1, NULL);
		LineTo(m_hdc, x2, y2);
	}

	void PaintDC::drawBitmap(int x, int y, Bitmap& bitmap)
	{
		bitmap.draw(*this, x, y);
	}

	void PaintDC::drawBitmap(int x, int y, const Bitmap::Ptr& bitmap)
	{
		drawBitmap(x, y, *bitmap);
	}

	void PaintDC::drawBitmap(const Rect& dst, const Bitmap::Ptr& bitmap)
	{
		bitmap->draw(*this, dst);
	}

	void PaintDC::alphaBlendBitmap(int x, int y, Bitmap& bitmap, int alpha)
	{
		bitmap.alphaBlend(*this, x, y, alpha);
	}

	void PaintDC::alphaBlendBitmap(int x, int y, const Bitmap::Ptr& bitmap, int alpha)
	{
		alphaBlendBitmap(x, y, *bitmap, alpha);
	}

	void PaintDC::alphaBlendBitmap(const Rect& dst, const Bitmap::Ptr& bitmap, int alpha)
	{
		bitmap->alphaBlend(*this, dst, alpha);
	}

	void PaintDC::drawIcon(int x, int y, const Icon::Ptr& icon)
	{
		icon->draw(*this, x, y);
	}

	void PaintDC::textOut(int x, int y, const std::string& text)
	{
		TextOutA(m_hdc, x, y, text.c_str(), (int)text.length());
	}

	void PaintDC::textOut(int x, int y, const std::u8string& text)
	{
		textOut(x, y, utf8ToWStr(text));
	}

	void PaintDC::textOut(int x, int y, const std::wstring& text)
	{
		TextOutW(m_hdc, x, y, text.c_str(), (int)text.length());
	}

	COLORREF PaintDC::getPixel(int x, int y)
	{
		return ::GetPixel(m_hdc, x, y);
	}

	void PaintDC::setPixel(int x, int y, COLORREF color)
	{
		::SetPixel(m_hdc, x, y, color);
	}

	void PaintDC::selectStockBrush()
	{
		m_oldObj.push_back(SelectObject(m_hdc, GetStockObject(DC_BRUSH)));
	}

	void PaintDC::selectStockPen()
	{
		m_oldObj.push_back(SelectObject(m_hdc, GetStockObject(DC_PEN)));
	}

	void PaintDC::drawText(const Font::Ptr& font, Rect rc, std::string u8Text, const PaintDC::TextFormat& params)
	{
		HFONT oldFont = (HFONT)::SelectObject(m_hdc, font->getHFont());

		UINT format = DT_NOCLIP | DT_NOPREFIX;
		if (params.hAlign == TextFormatConst::alignLeft) format |= DT_LEFT;
		if (params.hAlign == TextFormatConst::alignCenter) format |= DT_CENTER;
		if (params.hAlign == TextFormatConst::alignRight) format |= DT_RIGHT;

		if (params.vAlign == TextFormatConst::alignTop) format |= DT_TOP;
		if (params.vAlign == TextFormatConst::alignCenter) format |= DT_VCENTER;
		if (params.vAlign == TextFormatConst::alignBottom) format |= DT_BOTTOM;

		if (params.lines == TextFormatConst::multiLine) format |= DT_WORDBREAK;
		if (params.lines == TextFormatConst::singleLine) format |= DT_SINGLELINE;

		int oldBkMode = ::SetBkMode(m_hdc, params.transparentBackground ? TRANSPARENT : OPAQUE);
		COLORREF oldBkColor = ::SetBkColor(m_hdc, params.backgroundColor);
		COLORREF oldTextColor = ::SetTextColor(m_hdc, params.textColor);

		DrawTextExW(m_hdc,
			(LPWSTR)utf8ToWStr(u8Text).c_str(),
			-1,
			&rc,
			format,
			NULL);

		::SetTextColor(m_hdc, oldTextColor);
		::SetBkColor(m_hdc, oldBkColor);
		::SetBkMode(m_hdc, oldBkMode);
		::SelectObject(m_hdc, oldFont);
	}

	PaintDC::Ptr PaintDC::createCompatibile(Bitmap& bitmap)
	{
		HDC dc = CreateCompatibleDC(m_hdc);
		m_oldObj.push_back(SelectObject(dc, bitmap.handle()));
		m_oldObj.push_back(SelectObject(dc, ::GetStockObject(DC_BRUSH)));
		m_oldObj.push_back(SelectObject(dc, ::GetStockObject(DC_PEN)));

		auto res = std::shared_ptr<PaintDC>(new PaintDC(dc));
		res->m_requiresDeletation = true;
		return res;
	}

	PaintDC::Ptr PaintDC::getFromWindow(const Window::Ptr& window)
	{
		auto out = PaintDC::Ptr(new PaintDC(GetDC(window->getHWnd())));
		out->m_requiresDeletation = true;
		out->m_callEndPaint = false;
		return out;
	}
}