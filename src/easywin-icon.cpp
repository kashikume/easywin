#include <cassert>
#include <memory>
#include "platform.h"
#include "types.h"

#include "easywin-icon.h"

#include "easywin-bitmap.h"
#include "easywin-paintdc.h"

namespace easywin
{
	Icon::Ptr Icon::CreateSystemIcon(LPWSTR id)
	{
		auto out = Icon::Ptr(new Icon);
		out->m_hicon = LoadIcon(NULL, id);
		if (out->m_hicon)
			return out;
		else
			return nullptr;
	}

	Icon::Ptr Icon::CreateResIcon(int id)
	{
		auto out = Icon::Ptr(new Icon);
		out->m_hicon = LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(id));
		if (out->m_hicon)
			return out;
		else
			return nullptr;
	}

	Icon::Ptr Icon::CreateFromTransparentBitmap(const Bitmap_Ptr& bitmap)
	{
		auto out = Icon::Ptr(new Icon);

		assert(bitmap->handleMask()); // the bitmap must be transparent

		ICONINFO ii = { 0 };
		ii.fIcon = TRUE;
		ii.hbmColor = bitmap->handle();
		ii.hbmMask = bitmap->handleMask();

		out->m_hicon = ::CreateIconIndirect(&ii);
		if (out->m_hicon) {
			out->m_requiresRemoval = true;
			return out;
		}
		else
			return nullptr;
	}

	void Icon::draw(const PaintDC::Ptr& dc, int x, int y)
	{
		::DrawIcon(*dc, x, y, m_hicon);
	}

	void Icon::draw(PaintDC& dc, int x, int y)
	{
		::DrawIcon(dc, x, y, m_hicon);
	}

	Icon::~Icon()
	{
		if (m_requiresRemoval)
			DestroyIcon(m_hicon);
		m_hicon = NULL;
	}
}