#include <string>
#include <memory>
#include <Windows.h>

#include "easywin-font.h"
#include "easywin-utf8.h"

namespace easywin
{
	Font::Font(const FontSetup& fs) : m_setup(fs)
	{
	}

	Font::Ptr Font::Create(const FontSetup& fs)
	{
		auto font = Font::Ptr(new Font(fs));
		font->m_hfont = CreateFontW(
			fs.height,
			fs.width,
			fs.escapement,
			fs.orientation,
			fs.weight,
			fs.italic ? TRUE : FALSE,
			fs.underline ? TRUE : FALSE,
			fs.strikeOut ? TRUE : FALSE,
			fs.charSet,
			fs.outPrecision,
			fs.clipPrecision,
			fs.quality,
			fs.pitchAndFamily,
			utf8ToWStr(fs.u8FontFaceName).c_str()
		);
		return font;
	}

	Font::~Font()
	{
		DeleteObject(m_hfont);
	}

}
